const path = require('path')
const { createProxyMiddleware } = require('http-proxy-middleware')
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  // module: {
  //   rules: [
  //     {
  //       test: /\.s[ac]ss$/i,
  //       use: [
  //         // 将 JS 字符串生成为 style 节点
  //         'style-loader',
  //         // 将 CSS 转化成 CommonJS 模块
  //         'css-loader',
  //         // 将 Sass 编译成 CSS
  //         'sass-loader',
  //       ],
  //     },
  //   ],
  // },
  lintOnSave: false,
  publicPath: "/",
  //开发模式反向代理配置，生产模式请使用Nginx部署并配置反向代理
  devServer: {
    port: 8080,
    before: function (app) {
      app.use(
        '/graphql', // Change this to match your Dgraph GraphQL endpoint
        createProxyMiddleware({
          target: 'http://162.168.1.135:8080', // Replace with your Dgraph server address
          changeOrigin: true,
          pathRewrite: {
            '^/graphql': '', // Remove the "/graphql" path prefix
          },
        })
      )
    },
    // proxy: {
    //   '/': {
    //     // target: baseURL[env], // 服务接口地址代理
    //     //远程演示服务地址,可用于直接启动项目
    //     target: 'http://162.168.2.219:8080/graphql',
    //     ws: true,
    //   }
    // },
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  }
}
