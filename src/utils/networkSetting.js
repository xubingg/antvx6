// 画布基本设置（这些例子上面都有）
export const configSetting = (Shape) => {
  return {
    grid:false,
    scroller: false,
    autoResize: true,//需要启动scroller才能让节点扩大画布
    history:true,
    // translating: { restrict: true },
    mousewheel: {
      enabled: true,
      zoomAtMousePosition: true,
      modifiers: 'ctrl',
      minScale: 0.5,
      maxScale: 3,
    },
    connecting: {
      router: {
        name: 'normal',
        args: {
          padding: 1,
        },
      },
      connector: {
        name: 'rounded',
        args: {
          radius: 1,
        },
      },
      anchor: 'center',
      connectionPoint: 'anchor',
      allowBlank: false,
      snap: {
        radius: 20,
      },
      createEdge () {
        return new Shape.Edge({
          attrs: {
            line: {
              stroke: '#ffffff',
              strokeWidth: 2,
              targetMarker:null,
               strokeDasharray: '5, 5', // 设置虚线
            },
          },
          zIndex: 0,
        })
      },
      validateConnection ({ targetMagnet }) {
        return !!targetMagnet
      },
    },
    onToolItemCreated ({ tool }) {
      const handle = tool
      const options = handle.options
      if (options && options.index % 2 === 1) {
        tool.setAttrs({ fill: 'red' })
      }
    },
    highlighting: {
      magnetAdsorbed: {
        name: 'stroke',
        args: {
          attrs: {
            fill: '#5F95FF',
            stroke: '#5F95FF',
          },
        },
      },
    },
    resizing: true,
    rotating: true,
    selecting: {
      enabled: true,
      rubberband: false,//鼠标按下选中
      showNodeSelectionBox: true,
    },
    snapline: true,
    keyboard: true,
    clipboard: true
  }
}
/**
* 节点预设类型
* 0椭圆形: defaultOval,
* 1方形: defaultSquare,
* 2圆角矩形: defaultYSquare,
* 3菱形: defaultRhombus,
* 4平行四边形: defaultRhomboid,
* 5圆形: defaultCircle,
* 6图片: otherImage
* 到时候通过传入的type===通过匹配 data里面设置的type获取到相应的节点设置内容
* 编辑的时候也可以通过节点里面的data.type 获取到到底是什么节点进行响应设设置
*/
export const configNodeShape = (type) => {
  const nodeShapeList = [
   {
      label: '卫星',
      data: {
        type: 'defaultSatellite'
      },
      shape: 'image',
      width: 40,
      height: 40,
      attrs: {
        body: {
        },
        image: {
            fill:'#ffffff00',
            width: 40,
            height: 40,
            refX: 0,
            refY: 0,
            xlinkHref: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACnJJREFUeF7tnXtwVFcdx79nH3lsSALZPDZPyJKYtglQmdHpUCulQp3qUGuLiLbi2HYq1VKhtP7hOI76p1LDhFLqdKojUuq0RSsdRy0IUhxGxSmSZLGBZBPYhDwgz82DJLv32HM3d3PvPu/j3E3K7P2HIfc8fuezv3PO7/zO75xLsJBPW1sh7DOfBGgdYK0DQR0oXCDIBWguKPsXAIEfIH5Q+EHQB4o2INgGkDbMZpxHXd2NhWoGSWnFvrPZmM3bCCLcB2q5D8AqEY+xhwJoARFOglpOwj52ApXrpowVqT63UeGT10QpQVfLZwHrdlC6BUBe8kyGUoyBkLeB4CGsWPU+CGGATXtMA1hz+XJmu+Xmt2AhL4DCbVoLEhVM4IVAf14jZP26vbZ22gwZ+ANk3TSQtwPA86AoM0NozWUSXAOwF7axV3h3b74Ava2bQUgTKF2huZGpyEBIFyh9Fu6Gd3lVxweg9+JyQGgC8CAvwUwu5xgo2YmV9VeN1mMcYEfLwyDkNQBLjQqT4vwjoPQJrFz1eyP16gfo8WQgG3sButOIAAufl+zHFJ5Hff2MHln0AfR4CuCg74JinZ5KF10egrOYJJtRXz+kVTbtAC9fqIDN+ldQ3KG1skWdnuAiAsHPo3ZNtxY5tQH0XmBLruMAKrVU8jFK6wOCm+Be06ZWZvUAmeZZrWdvYXgSMx+CwXVqNVEdwNCYd+aW67bx1Ix150lyj5oxMTlANts66KlbZsJQ3zfZxLIh2eycHKDX0/TxN1XUUotMR/bDXf9s4uV2orchI/mo3upTlW9DRiZGBQEfBGb5V0npI4mM7fgaGFqe/XexrzAOO114NM8JART7hvqxZ3SQN8QRULIm3rIvAcDWP/Ja2z6SlY3Hcwvwr5sT+Kl/hFsD33C6sC3PqShv31AfdvOHeAzuhi/FEjw2QOZVAY7xaOm3HUvQVFyFDBKq6sjYIB4d7DNcdCx4UqEmQXwwlhcnGqDoz8u/yMMlFQlPaqBRiIngmQaRucJso3dE+hOjAXY1PwfB8qJRFYkHzyhENfDMg4jnUN3QKGejACi64W3TXqOe5GTw9ELUAs8UiATXagKZbvn2gFIDO1p2gJCDRrRPLTytEN9wlmJbXoEu0biOiZQ+jZWrXpEEmQco7p552o1sAGmFpxaiEXjcNZFtVK2or5F2++YBdjavB7X8XddP/NHGLDNVjriqw7Ot1nLiTSw84PGHKNyL6tWnWbkygJ7XQOnjWhsupf9TUTm+sMSYVz8SIk94kpzf7OvCoakJvc0M5SPkV6iuf2IeYChigBlnuje9f5S7FD8pLDcmmMxONAMeE+7g8AC+M3LdqJxjsI+5mEkT0kBOhvPrThe+HrEy0CPppZkpfCIjW0/WhHmGhADW93SgNRDgUbZoWIcAdjY3glp28SiVF0QessjLYPC29Hbh1AynAAUi7EP16t2SBl4AsJqX0IsNInd4IVDNcDesIRBDzGYHOERJKfgvFogmwWNtpZi1FxN4WzYB5D1e2icvZ6EhMnhbe7vwN17dNgoSvZ8B3AkQFpZhyrNQEM2Hx3AJOwm8zS8Blu+aQm+u0FRDTA08EeABtnw7DoFuNBMgKztVEFMHD4CFnCDwepoBykJtTX/MhphSeCIt0kLQ6enk4TxVS7+t3G2akbyj/yqqbHb0B4M4HGO5lmOxYocjJ+57tW0IpyOki6Cz9QYolBsLmktSl8Gs5RnTvO8NdKOxqByFVrsozDv+YXz5BgtMDT232Ww4U+aO+15dCyJSEQwSeFuZaZ6hqwANmcyCNy4E8VBvJ7bl5OPJpUUKiW73teHDuWXbq8uKE77X0BR50pmUADQLHmvJW2ND2DrYi/eKK7EpR+kLube7HadnQ0u3WO8f6OnAX2Zu6mQnZpsxvQubCe9GMIC7ezpwKRjA+dIVuDMrRwGDXPkfIAji32K9/7TvEs4Z2YwXu7CJk4jZ8B7o7cR/ZllgKcFoVR3yrNYwwClBgIMBnHuuVtai0qYcqeSAdamhOImYZMakDh6wPTsHv3EpDwZcuDmBO3u75oEuvx1ZFkv4/2zicV5RHQYYhy8zY0wwpFMJj7XsREkFPufIVzRSvpH01SwHfldarXh//uYE1soA69TA49yXcqmGx/Zi3i6NPgh1d/dlnBW7N3CooATfyC9UMDowPIBnDHumyUsM4DOAZb+uXyAiU6rhlVit+KDMjbKIse0fk2O4p98Xlu5KRQ2q7JkKaTf2tHPw0ojOBD7urIWAd6KkCg2ZDgUYFqV1f09HGM7Xshw4EtF9fYFpVPnaOegMc2dxcKiatcZlRvKfx0fxw5HroqkiPazbNhVVRGme3C6U0raUVUdBfn10EI8NGQ5wmnOospq8rbpd+iy48WR5DYdfU1lEtGOAYHu2A9vzlkVNGFJOcebtuwLQ0AnXXTl5aCxWHigIUooNPe04Mzc+GhB8zqXPSjCwqbTWZse5ylpYDJ+bnm+KtLZdn5WD5fZMFFmtcNuzFHZeZMOvBWZw1zUvfMGg+KrQYsWF8ujx8ah/GFtka2TdACM2lQzFAzbmO7GrwKVbFnlGBo95VV4urgwv/JMVzDRv84AvDI8Z1v92VeFT2UsUWZlx3dB9GV7ZcJCs7ATvZduaHDbWeUCUuu3ajCz8rKgiadvYhHF0bBhb2Xg2121ZpjedpfhKjECkV0eu46lhtn9m+InYWBe7sbHQDlaEEYjyMe+x7Bz8NmJlEdlkZqr8eHggwhQheNPpignPMz2JBmY4y0DrxhgV2hEaBw0FF0nC6IEYy5P8h8IyPJS7LNxG1v1YxMKpST/emvSHjWQpAXOWniquiOq27P314Cw+0+NVzOS64bGMJFZwEYfwNj0QE7nhmRO0hFhxOjgb9qrEajibbV8oKIlp1kxTAVt7O3Fs2pDbar7auOFtLAmHAEstEI3uYTAj+QcFJVF2niQDg7fnejcOTPgNKZwic9wASwC8QnzVQNQLjzkGvujIxXpHbtTyTN5Q1m2f7L/KT/PErpssxFccC1t3g+IXvH6yWGOiGnis++7JLQjbgU6bHUUWm8IlFU9GNmE83O/jN+ZJFZEkQeZiOo7HHGJpohp4bELoqlip2g6U6mETzeGxQTzFvCw8Zlv5L6T6mAPLxCleUF4/c3relenAy+PDSePz9izJw94i9We62fLsnfERfH94gJeRHEu5VR60kbJ6+R310jocqLEDxc4SmMH7E3780j/EY22bSEyNR71ELVzYw4aRdiATiXX/KzPT+OfUBI5+5PMzL+pKwVLnYcOQWbOgx13ZRLLCYsOgEMQ5IZjQFtSq5arT6z7uGu7K6QPXiWAnP7GePvKf8EKe5AAZ/vSlE3GVUB1Alj197UlMiOoBijNz+uKdSIraAEqamL76KcxRO8D5MTF9+ZihsyHp6+9ELdSngfKBIH0Bo2qbPH7C9BWgHCBKXpz0JbQGYTJ/YnDJ06CWPUYvrzAoyXx2dg0yEV6Edfzg4r4GWdbi9EXcvH7+9FXwvEjObRekP0bAEWj4cxi4TfwkhsVaBwElCT+HYUE/hLlPYQAfLvTnMP4P64IweATwpLMAAAAASUVORK5CYII=', // 使用不同的图片路径
          },
        label: {
            text: '卫星',
            fontSize: 12,
            fill: '#fff',
            refX: 0.5,
            refY: '100%',
            refY2: 4,
            textAnchor: 'middle',
            textVerticalAnchor: 'top',
        }
      },
      markup: [
        {
          tagName: 'rect',
          selector: 'body',
        },
        {
          tagName: 'image',
          selector: 'image',
        },
        {
          tagName: 'text',
          selector: 'label',
        },
      ],
    },
     {
          label: '指挥所',
          data: {
            type: 'defaultYSquare'
          },
          shape: 'image',
          width: 40,
          height: 40,
          attrs: {
            body: {
              stroke: '#fff'
            },
            image: {
                fill:'#ffffff00',
                width: 40,
                height: 40,
                refX: 0,
                refY: 0,
                xlinkHref: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAADBpJREFUeF7tXQl0VNUZ/u57s2SSSUggJCFkIEAwGhRxKVWquBwQtTC44jl1qWA5Qm20gNQtiQhRK3WpIGI97ltFrUBCrQiCa6S2iiJEIhAmCUnIPplMmCXz3m3vCxmSkGRm7rw3A55cDicnmfsv93t3+d+/3CGIYltWNivZG0POEimyASEbRM4GkAaKeIDEU7CfAAHaANoGwn7iMKhQBshlEkGZwU13LssubozWMEgkBS+qut4UL7umEVm4VCK4FCBnENCwdKAgFKA/iBTbqCBvaxNMW5+yvOuK1LjCUj4YJSkFKTh09VRQegtkeh1AE4Kh4+9DHBDIeyDkteUZ6z8jBJSfV2BKzQDM3ZdrTDRWzIWMpQDGBlZFkx7lEPAXu2f0y6vHr/ZoIUF1ANkyjfO5FxAi3A3QdC2UDp0nqaFUfrxdF/Oc2stbVQALKmbOolRYBSAz9EFGhMJGiHzn8tGbitWSpgqAD5RfOZqIulWEwqqWYlryoQRFos+Y+9C4dyvDlRM2gHk26zUE5EWAJoarTGTpiZ2C3laYWfR+OHK5AXxnT45hV9y4xwlIbjgKRJuWgq6e2H7g7jkTSr08unABuKxqxlCfbCwmFFN4hJ5oNJSgRCd4Zi2zbG4OVbeQAfzTvmsyjHppM0BzQhV2YvcnpZ4OccbK8e8fCkXPkADMK788G6JxC6HUEoqQk6UvJaQKkmd64dgPy4LVOWgA2cwzGKSSnyt4XYAxEL1ecUqwMzEoANmeJ0kxn//8lm1/84yUiqL7wmD2xIAAKqetOWv7z+XACHZpsoNlonP/JYFO54AA5tlmrTrZTZVgQevdj5k4hZnFdw5EPyCAnUYy/sGrgNZ0BAISdakYrs+AgcRAJDqIRK+IbZdaYffVocFXA5lymXgKHwpcO5Cx3S+A7PVMEPTfqfGGMXtYLhy+Ruxs3w6773BYuJrFJOTEno9s07mwGE9FjBA3ID+ZSmjwHUKley9KHBvQ5KsJUT6xC5LhzP5e+/oFMK/CulGtd9vLEm/FBUOuVp5njfcgyl27UOEpRY33ANqkgZ3JMYIZ6YYsjDKehvGmScgwngI283jal60bsdn+Usik7N25cHTR7L4I+wTwqFelKGRJ/RCcHjcVc5KX9PmpRz4Ch9SMdtkBmfpAQWEgRhiFWMQLSTCJildflVbbYcPamru4eBEiW/vy4hwHIPPnmSVPqZouqSTdCCwa+RyX4uoSUaw8NBdOqYWHrc0pGnN6+xOPAzDfZl0M4AkeCQPR3G95K+B+pbbMvvitb1qNnc6tXKIopYsLxxQ/1Z24B4CKG15fWa6FJ3lu6sMYE3M6l+JqEu1q/wzvNfLOD1Jj7xg1tnt4oAeA+ZXWBZCxVk2Fu3hdnvQ7TEmYpQXrkHg6JTtWHvptSDQ9OgtYuGJUkX8/8gOoRM8qrPu1CgBNirsE1yT/kV9xFSmfq70bNd59vBzLl48uyuqK9vkBzK+6+iJI0ie8XAPRDdePQm766kDdIvL5xy1v4lPHO/yyRPHiFZb1nzIGxwCsvOpFyPI8fq4DUzLb7QHL2zAIRq1EBM33oHs3Xq57IOj+x3UUhJdWjNpwmx/ATtPFe1jroPf8tJWwGFn2RnSbRH14pOpGdFA3pyLE4RQNacykUWag2oZzf1rNHLoAk+Ov4FRaXbI36x9Bmevf3Ey7DGsFwAcPWp+SCTTf4c82z8BVw37PrbSahF+3/QubmvmNe4Hirw+NKVqkAJhns35PgIlqKtgXrxGGcVg44kmtxQTFv9FXjVXV/A+TArsKM4vOJCzFzGcU6sPNkgpGa4GIyLes87ucgqHRss+T1bdze4dYVpjOI6eQ+yus00WKj7RUtDvvBSOeRLphXKTEDShnU/Pf8HXbB9y6SASXkYKKq3IplVk+S0Qa8w2eY54WEVmBhPx4ZAf+3vBooG4DfZ5L8mzWZwhwRzhcQqGdHD8TM4fOD4VEs75uuR2PVt0ECplLBgXWkLwK6xZCEbEpYTGehvlpf+ZSWAuiFw7fh0rFexd6owRbSX6FdRcozgidnI9CRwzIs6yDQPi8yn1J9coutEj1cEqtcEtOZUaxf0wWi5UYBRPMwhDEigmKo7Z7+8T+Lra1vsE3GIIfSL7NelBN52kwmtyRvgqp+tHBdO2zj8PXhAPu73HI+xOqPGWo89qCXoYsRJCkS8UwXbryX0IHvnBwJ2jZSL5tdiNAh3GPhoPwuuTFmBh3UdCUEu1AlXefEkspc32NWu+BoGm17UiaSJ5ttoeAGrQV1JN774Nkv+tbCEQPZidSKsNLXcpybPbVodrzk7JH+cIITWo1NgrijQqAmcbTMS/tYf+41tYuQa2XuSJPrqYAGI0lbBDikGd50+9NCydOEV3ISVNUDhE26MUjX0Cibrgy/hJHET5seTG6WPBJt0XcjOnS8+aUAow3naP8yk7UV+sK+IYQTSpmxkTakO4a7/TEW3Ghkq0AOHzNeLx6bjSh4JNNsCXir3Jdmp4ZdzGuTV7kV/zRqpvhkh18A4kalfAM2wP/ACDi0Z5UfSbuSH/aP/RX6h5Eufu7qEHBKTg34u6sLkUJRBSMekdJSWPtg5aXsMOxkXMc0SFT3FmRdKj2HiabgWwmsrbT+THWN0XMqxY24n6HKuMUKZd+b62vS16CiXFTlT+z99rna1lh58nR/C59pm6kgkq9oblwyBxMT7xR+TPzqBRW/eb/MUI+31ykYe8RVIpUWLP3IE8xTcZNKccC3E9XL+TIII00dJ3yeoQ1IxVY7z3UeDEZSzOOvYG83fAYSo+URAeRkKT2Cqwz2nyNUzv60+8+yxswCZ1ZqNvt67C99a2QhhKVzr1TOxQANU4u6m+g89IeQaZxgvJx6ZGv8HbDiePu7/fh9JVcpHV6W3/KdE/3CDfYHaHZ2Hd6W+cy1i7Bsr/BTY6/EjOH3q58LFMZhVU3nJDOU7/+/SVYsg5apvj2B6DFmIP5acdisyz50SU70SrVg9V4nFgtQIovUzbv4KxFhBBNElgEosOppsnIMGQj3TgOKXoL1tTchXssr/idqxua1mBa4o2IFRLgkBrQ4mtAvbcC+93fY597Z1hVR+E+jIBJ5kyAFmUOibo0nBf/a0yKu1gJLbLGcpXrOiqxsWkN5qasQJI+Rfn7DkcxGn01ShXS0KPRsy4al9SGb53blOxSt+wMF49Q6YMrc2Bc1TSsfxF/BS5Pmgc9MYAVuuxyfoLd7V+gVWrwD+CWlAeRZTpb+d3m2YNX6gqUopvOJihVSqeZfomzzZcqhTcs4LSucSUq3LtDBYG7f9CFNl0S1Cj1mhB7AW4YvlQB40P7q9jh2OR/VWN7368SZmNMzAS/Hdglmx0mDb5K7HaW4Mu29f5DhcV05yQvRZZpEliF05rau2D31XODEixhyKVejLEaxYa3phZibMwZ+E/bZhQ3P+vX10hicZ/ldbA9MVD7vPU9bLG/7u9mEhJwr+U1EBB8ZH8NX7RqXUzKWWyoHChhlrveMPxeTIg9H/tc3+D1+uU9sDovwYqpCdfCLPZ93QyrmWPx4uLm53vk8KUbsrFgxEqF14amZ/Gtc3OgZxDW59zlrv6lHEbBdZphLOanPabsf984t2Jzy8u9Nn8BqXoLUgyZyjLWER3cSvFhEyo9e+GV23sMPid2imIzMtBrvOV44fA9mtqMYRdcM+3DLfnPMJyC65OXIEmfpris9hz5Cj+5vkGl50e0SU0Dzg621NONWRhnnIgc8/lI1mUo/fe0l2B98zPHARzWVOtFrFrJP+Mb7qUTLGXjXPMMnGWehpHdslOZKdIutcFNnXBJ7aCQlIJEdliYBDPixCHKXscaOzTKXP/FV23/RLVnr5pY9cFLxUsnurirde0JswlZ0eFIQxbixaGIExMQKw5BnLKEjWC2nlNuhUtuQ7tkR2NHrTJbI5Ufo8m1J/79cPDineNma8BbO3pTDF791BORkAHs2hMHLx/rBJILQP/pPHj9HT+Ax+zEwQsYwzYJBq8ADRvCTgaDl9CqAORRf+JCgCzR4vIKPhVJDUCfcIrGtSf0NcjdBzd4ETffoz6OavAqeJWAZGwGv4xARTAVg/zo12HoIJwqUzkbBNkESB3o6zAoUAeKMoEIZT7Ie6P9dRj/A7GTQ+x9o6eWAAAAAElFTkSuQmCC'
              },
            label: {
                text: '指挥所',
                fontSize: 12,
                fill: '#fff',
                refX: 0.5,
                refY: '100%',
                refY2: 4,
                textAnchor: 'middle',
                textVerticalAnchor: 'top',
            }
          },
          markup: [
            {
              tagName: 'rect',
              selector: 'body',
            },
            {
              tagName: 'image',
              selector: 'image',
            },
            {
              tagName: 'text',
              selector: 'label',
            },
          ],
        },
   {
            label: '通讯节点',
            data: {
              type: 'defaultSquare'
            },
            shape: 'image',
            width: 40,
            height: 40,
            attrs: {
              body: {
                stroke: '#fff'
              },
              image: {
                  fill:'#ffffff00',
                  width: 40,
                  height: 40,
                  refX: 0,
                  refY: 0,
                  xlinkHref: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAADW5JREFUeF7tnXd8VFUWx793ZhIEQgmgBAWkGgSluygrJVnADzZcFcuqoKAUFT8IrOy6Lrp+XAsCuoCKhQXR/VgQP2vlo2DQBFGkBZASpaMUpROlJDN39859k3lMprwWUD95f03yzj3n3N/ccu7vnPdGcAqvngtkPZlGBxEiO+AjW0qyg5IsH9SQUMMvqKHcC0oOCzgcgsN+wS4hKCoNUSR9FIkSVnyaI/acqm6Ik2m4/yJZ9WCQXqWSXCBXCM6X4MoHAVJKVgN5AUFeLT/zZ3cVR05Wv1w5b8lJKUXvArpLwYCQ5FoENS21cyokOeQTvCUks+Z1Ix8hpFNVVtpVGIB9P5RVSjO4LQh/RtDMijOey0g2+eHJQDEz5l4qjnmuH5fTJ55DapruL2VYCMYgOLMinLatU7LDBxMyA0zzenp7OgL7FMgrSiWTETSx3cmT0UCyJSC45+Nu4j2vzHkC4KWfyLOPpoWBu9IrxypUj+TdY4IRn3cT29zacQ1gzkJ5NSGmC0Ftt86czPZScgAfgxdcLN52Y9c5gGtkes5+JgjJCDcOnOq2UjBlQSZjaCOOO/HFEYBtFsk69Ut5D0FXJ0Z/cW0ki3YHuGJNV7HPrm+2Aey7QDY87ucjKWht19gvWV5I1qYHuWRujvjOjp+2AMzJk9kEmCcEjewY+bXISsl2Sum9IFcUWfXZMoBq5B31s+i3Cl4EMAXiaUG6Wh2JlgBUa15WKQW/tWmbaJSp6bwrQDcra2JqANfI9Ny9LPjNbBhW56ZkUV5dclLtzikBzFkoJ//aQxWrmMXKhUOci8U9ydonBVAFyUIyx4kD9zWH7UfgjR0QcqLAgzY+4PozoVFVGL/RmUIpuCZZsJ0QQHU8O5JGoZMTRvsMmNBeMxUrD8DYNVBSoaRSeXDSBDzRBtrVBmV6TCEUFtsHUZ1YjgvaJTr2JQQwN1++4/Rs+1w7OCfMJcOyfXDfWvuOe9FifGvoVEdr+uYwDF/pUKvk3bzuol+81nEBDLMq8K4Tc+1rwMR2umVIwt3Locjgh9WUqhGAg6VONKdu4xdh+r/syq4KUzuCz+jl6JVQeDi1nngSAbgyHotTDkDF5+0tZa1TSuqBlpBTX7vwyW549NuoO/3qw82N4V/fwsIDzjqSqNXFteHOlnD/Kthiok7/2gJ6ZcX3x5YHki11A7SO5RPLAZhTIEcJmGhLuSFc3QezL4QqaqgBo03rTjU/vNoZaqXpNemxdfDJXidWyre56Sy4ralecxfvhfvXRWXaZcCk9vrvYyHo/yX85HBX80lGze8unjJ7cAKAioY/lsEmp0zyhbXgn+dr9fuPw3VfRXfga7NgeAt978BxGLgMioMQO+3sQBpp26kGjDeWDfXlDFoC24xRqL7LN38Hmela899Ww5cH7VgxyUp2VCmmmTk9cAKAvfPlsKDgOYfquaMx3NBYt/54FzyxIarphfbQPEP//dImeG2H/vx4a9hUDNO3n7h+JfNBATe4ETSvDmON0fZka+hobBhvbofnt0Y1jG0OfRrov1/fBi+6oFH9kuHzuotpEe1RAKUUuQVscJMAmtAGOmRq1RPWw1wjW5sZ0FNbGVMj5IbFsKcEzs+Ap43pVXQY7l6ZOmZUI2pqO8g2dvmRhbC6GK44HUZma9uxO27fejCmlb63Yj+MWeN0iIQ7sCmvGy0i2b4yAHvnyx5BwacuVDOjIzSupjWMXQVLD+nPPevA3w3ySwF1pxFOOF3gzRvV/F3w2AaoFYA5xpekduJ+X8ARY63rXBOeaKt92fozDFruppfgl/Sc1118prSUAdirQE4PwSA3qt8yrTWDl0R3w+sawNDmWrN5Z36mLbQyssRmwCM+tK6uP6396USvzICsPwR3rdL3E9lvXAVmXKBl1Np87Vduegk++Pf8bmJwGYBG6LLLbdL7o64QMHbgyxdFR4B5bZzzHTy7RXfgnS6QkaY/918M+0qiHcutA/cbo/bRdZBn2rHrpMHsLlq2uAT6LdafX2wPzYx11hwBVPXB+wZ3XhqCSxa5AxDJoboBslRIEx6BbgJnsysfXASn+fV/rv4yGjDbBVAdw2Z2gqzTtK5dR+HWZdHjoF0A1fR++0Kt62gQLvvCJYBAJLAOA5ibL59CMNKt2tc6wxlGp4ctg2+NE4jdKXxDA7jDmPIRn8w7t90p3LIqTOukNf1wFG5c6ran4c3k6bzu4t4IgCsRGMusc+XmUOWB1fCFEW/Z2UQy/DDLCLjNnhwsgQFLdexodxMxx6cbi2FIofM+lrWUrMrrLtoJVWLmD/CD2yoppfjhVvD7etrEsxtgzi792U4Yc3tjuN6IJWO7+cZ2eGmr/TDm6vpwV0ut7fM9MG69ewBVVViwlDNEj4Wyt1/ysXuVcOOZcLtRRhR7pDKPzhc3wus7tUVzIK2AfrlzdB2N9UmtX7cuhX2l9gLpR8+FLnW1NvNS4LbPQUEf0StfjggJJrtVptqrsGNKB63pWBCuWRzdia0c5UY3g0tTlCN9uAMmbtI2rBzl1A48pwtUMTa3ESvKh0VO+x6SjBB/KJBTJdzlVIm5Xey585G1sMBIVZvXNnUaiQ1NmlSBFzprUJJdKkgeugw2H41KJSMTcurAA0Y4FHs+d9tnAc+InAI5T0Avt8oi7YecDdcbWWNzkKvuX1UfbkpAZz2UDd1Ot+bFwh/hwZjMbSI6a2pbONcI1mPPyNasJZaSMF/0zJerfAKDQ3GrEhqkw6wLoiTm4+tgnhEEJyJUz6sOT3ewXqyoRvDIFfB1zAklltnpXRf+cq7ukyJ3ByyBnY4qYOLjEpKsFrn5crNT8jQR3OPOgR5n6Ls7j8JtpiA4XptJ5+nchZ1L5VpGfZ24hQrGZ3SCBkZc+tkP8PA3dixYkJVsETn5co8QGHuUhUYWROqnw/ROUNWvk0r/WJ+Yxr+oFjzicPyP+xo+T8Bsq9PHg630F3MkCIOXwW4PR5+CQUr2il4FUhG1Bt1oAR2LIur0Ed5UdiamqNT9aSae0KLqMjHFIw4tTK5f+aFIGeWH15cPjlcYgFacNfN0VuTjyZh5R6c6nLYLA1gRU9iKQ2rBn2UiDFT6c0PMppBIT4vq0XRlLNFgxbZXMuEpXBGbiBUHzQTD4VIYuNR6ulOtb+rEolKk6np+Y8VM0ZT9UJuI12FMSqOAytC90glqGyvvy1tglq2yRhjQEAYazwKoJNUty+DnoBXr3smEwxivA2kr7pn5QRXT/fd7OJ4g1bjuEBTsL6+1RyaMaxP9v9tkkRW/Y2UEzPP0KGfFiXppMLOzDnGSXWpUzdoK7+0+cZdtlA5Dm8FFBusT0aFCFUU0qGTVybp8gqlqCt/tE0w5WUZHNYPLkhAG6qz7/k6YuRUOmaakIgVubgh/bBhN3Mf6/MEOmGQQDSejP2EywUs6K5XTKrmjCAN1Soh3Fe6HaZuiTHZERh3JBjWNst2J7KgKsCFLo0n1VP64vR+ms7wkVFM59OA50N044plldx/TPJ05caTuq+KgYc2hrY1jXsGP8JDlEvFUHie+X0aoKpHcfOkJpZ/MHcUVTo4hDBRnqLJ0r36v61YilwpTBjXS3GCksspqV9WmdI+HnF9CuxFK3wDQk6RSsk5ObAPtjaoFJacoqWmbT2RH1NFOUV43N9FFSE4vtRSMdlN9YMWwOankVVozkV1zUkfRSg+tKU8CdKgBw5tH62es9CGZjKsiIgvGT0hrepVYT2RXEQYtjYR3bM2gYm6GNNH0V8qKdwsdi4h4ln2LZzM2sa5kvCjtiGerTz0YaxT2qGB50NLotK3phyuzoJpxJLOBT1xR9QVcdRakG9UR49fDRxXwOopypR3KGy+Ki2J7pQiDl02kpjpxTNnsFqbk7Uc01SCqS5G5qg7RXPbrhfW4xUV4UN4W61z/LBhmFFX+ZBAG+yuoPjpiO5IarW6M6mkbYbaXXGCi8jZjFLoqsDQDqMp9FWMSqQxV5OeSOGdaL0ZErI4LMqNFRl4TDYkLLP9fX+y2xNfcEVVB+qezKwIe+zpf2wYvuahKLbOYqsQ3vJnky3tDgkn23Yy2UNVTqr4lFWHgxoadtl4RDSmLzJVTbh9zUDrubQqXGwv5oRKYa9TI2Om0F7J9s6CmEZC//z085WYDs/qYg3LcTWDdsIrOyEUKLV/ZAjNtkqVegKd0DGwIAwzS1S3RYPlBm4jzTh/1MueE1ei7xShJ8woUO3oU8/2fztFR6Dg3bPdRL+Wkk4cNW1XTj1ZFThSvboUZ2+102XtZ8yhURMOI5bDuZ+t2HD9sqEzYfdz1yTbQMVM/yrB8n06oO30qyHoXk0uqcEol2NUzJOqLtfuYg+PHXSNuVT5w7eKB6zCIlY/8Jy0IsUSAVL50IvEyYQlA1bzytSfxQbQMYHhTqXzxTjkUbQEYGYmVr36K4mgbQNW08uVjLgGM7M6Vr7/zIA1R+QJGD0L+yleAegBiGYtT+RJad2gqPnFPkOFCMtrpyyvceRCntWSHFEys5+e5X/RrkM2uV76I26thUPkqeK+Q1OmCyh8j8A5PIj+HkRailRRkhwj/JEb9ZD+HIQS7fVAkJEUlPtaf6p/D+B8qZoMczScKXAAAAABJRU5ErkJggg=='
                },
              label: {
                text: '通讯节点',
                fontSize: 12,
                fill: '#fff',
                refX: 0.5,
                refY: '100%',
                refY2: 4,
                textAnchor: 'middle',
                textVerticalAnchor: 'top',
              }
            },
            markup: [
              {
                tagName: 'rect',
                selector: 'body',
              },
              {
                tagName: 'image',
                selector: 'image',
              },
              {
                tagName: 'text',
                selector: 'label',
              },
            ],
    },
        {
              label: '地面站',
              data: {
                type: 'defaultGroundStation'
              },
              shape: 'image',
              width: 40,
              height: 40,
              attrs: {
                body: {
                  stroke: '#fff'
                },
                image: {
                    fill:'#ffffff00',
                    width: 40,
                    height: 40,
                    refX: 0,
                    refY: 0,
                    xlinkHref:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAADJVJREFUeF7tnQl0VNUZx38vhF0QCYSwJmwGCcSiBjWooAiiCBxFE2zVCi6giMpij62VRS3VKoqAgloWtVYTwNMCWpVVURDwKBCyESBhUbZEVtkCvPK99yazz7yZeTODnLnncIDk3u+79z/fvd967yhEsakLacRxunCWVOJIRdX+JAH1UKin/a23I6gc0f5W2INCMWcpJo5iavOj0o/yaC1DiSRjNZfaqNwM3ITCTah0BkKdg4pCHirLgGUoLFGyOB6pdYU6eb/zVGV5c7kBlfuBu4D6fgeF1uEwMA+F97mbrxUFNTRyvkeHDUD1M2pyhMGoPH1uMW3CuQivtFW2ofAK9Zit3MbJcMzBcgCNbToMlTEoNAvHpAOmqfIzCq+iMMPq7W0pgGoO/VCZgkJKwIuMxACVMhSeULJZaBU7SwBU55PMaaYA/a2aWJjpLEBhhJLFjlD5hAygOpc7OctMoEGok4nw+IPE8aByN5+EwjdoANVcaqDyKjAilAmcB2OnojBGyeJUMHMJCkA1l4bAQlQyg2F63o1RWAX0U7L4JdC5BQygOp8WnOYLoGOgzM7z/gXEc4sykF2BzDMgANUPSKU6i1FoGQiT30xflZ1U0ku5j2KzczYNoCZ5lay6YMGzISYgVifTrCSaAlA781RWXoDb1pugFaBwvZkz0S+AmraF5ReMwjC/N0Wx3OhPO/sHMEczkH/rpopZ2Fz7TVWyecLXYJ8AGkby/GC5XxDj4hjoy9j2CqDhnq2PmocRD7QcpH8GOz+G01H7OA6icLk3t887gDn8N2q+bZycPt9BwtU6ahVrYPk1cDZqIC5QshngibtHALWoCiyI2nTTxkLaBGf2+eMh3+VnkZ1gf09RHDcAtXjeWUSNRyckJfHq3ichTpS/Qzt7Cr6sCRJvjkaTUFgcHV3jie4AfsyocwmdSdGYo8azx3JI7OGZ/f6vYLmX30VmwqOUbF53ZOUEoBaGP6yFwaMTSW49CDI+8g3Fuj9A6b8jA5crF4ls16eNY3rAGcAchgHTozI72bG3lUONBN/sT1XAZ40ILvhkycoeVbKZYaNUBaCWPcthS9QSQF1nQ8oD5la4/T1YY7KvOYrme0miKpt2tmyfHcBcuqOywjwlC3smpkGPTYERXNEF9omZGoWm0EPJ4ivhbAcwRwvLD4n4dMTmu6UI6qUGxvpoCXx+abRsw1lKNg9WAWikIvdEIOntDlLaXyHthcDAs/UumACbxgc3NrRRh8+lSJPEpNEkMGqGc3Xg9kNQPchihcpDsKgBVIaGRpCjNcPaBqDYNk8FSSi0YbWBtLcg5UF349kbZTGqt86AwifhRGjsQxg9WclmpA5gLhtQSQ+BWOhD6wiQb0PyYIgT0fTQzlZC2T8h/zEiVz7kZWkKG5UsLle0ErNj7LOgSso8iILPZS/B5mfcJaiuxL1nQsr9oEhIRj7h01D2HuQ/BMdc2IgEp74O+SMjvZVV6pCoqLn0Ohdt/tL86i3oedV0aDMMzhyHrdOhcDRupT8CZKc5oKqQPxh+deFbSyR2CrQeqm/9bTPg+0ctmFwAJBR6K+rHjEDRos6RaQmtoGeZs8CfOQYlU6HoGf8eRk2R3knQdjhUk//YmgpLU6Ai5GoN8ziojFDUHKYBw82PCqGnnLi98+DiTp6JnD4KJZOh+Dl3IMXV6zAR2j0B8SKeHtrBPFicTngrAp34vikALgatajT8rcNoSJdqED+t8jCUvA6bDRvv0glw6UiIt1X8+hi/cTQUveaPg1W/XyJbeCOKVmob/nbtR/YwvRluqhGCVsRdMdF+LYUNI2DXpyY6W9BFJU8ALI1o8PSS2tBpLjTta8EKDBIn90L+ONj2dmRdO5Uy2cJS4e4nhmTdWqsoNUzUtWzSrcETF0+kWMyhl9yTTqKlw29kVwiAUjvsEj8Pfk1uI8Xmu3E9bJ8FW6bAGZceCc0hbRYk9fbPVOzBvcugYiUcLYTd892VTeNU6Dwb6raC/7UIdzbvVPgB7PIGtDdy0yf2QNGLsPVNdyAbtdKBbNLTHUgxc4pehpLnfZs5ouXbDj1XNzYeaiVByRT48Un/H0zwPTQAw7eFGzaEnvvBVQmc2A2Fz+v+rGuqsnF73aVLvFFfVsVqWJVpd91EnzS+Eup3hRqJcOYIHFoD5d/aPRFxYCQ10CILljaAX+SOTlhaRfiUiEjDzT/AJV28z/z4T1D4gufDv3FHSLoH8p/TQdbcv4nQ9jGofrEHKT0JO96HTY/YwZZQWbM7YMmV4bENNSUSLjOm/aPQ5S1zH/vxnSCxvdKZnrWoaO7rSqB2c//0Kg/C2mz4yfBOM96Ggz9CSVUawz8Nsz00MyZchnTfUqgbYGr52A4oHAelc+xACng3lUM1CdcYTY6AfSvg+C6o0QASeznzUs/A6gG6PShbvttCWCm1Apa3xeFz5e486t3l8reOvD9B4Sv6tu2zyy55Il3yu63vum/JpEzoOg9qNdWpi+L5vK4ehJBzdX+JP66B/15hmmzhx1GYGvhoPyMy50ILuRoXYBPpWRSvn2PpE6HDn3UC4ievaOasEMTWk9p6myKS2ELvnVC7hT5m96JwSZ5OXwsmhCucJYvJXAGNuweG4J7P4etb9a034KBdYXx/H2z7l06r9e8h/TWo2QQkyLrjQ1g/WAczqSvcsMbgqcKnce6hsMBm5L23Fs6yKqAqWrdhEhzd4xzba9EHOk82n3UreAE2jYUmGdB9rT55Oes+balLmoCX8aH7ovYuhq8MY/z6hdD0dr3PD4/Alnf1f8uRcBFwwBIE9YCqJolWhPQ7TYCOY0HyFVumQcFou9ErXNo8AGl/g1p+qkbW3AXb54OjFpf6wNX36KsesEeXPE9tSRP4ZR+0GwpXGFq39B1YN1QHr98RiL8Iil6CjcbRECyWtpC+BmCOVjATWlKp2zxoPtA+HQlJFb8Mmyfa3alqQPsxcNmzUN3LzbCV18HubyFtHKQZ4azCiZD3LMiZ19/L9d/KA7AyA8q3QvPe0E2uskhxZg6sHqQDeIcx1lFagwUQHJJKVtQDSozzmlWQcK3zlE7uh4Kxzl6HZhT/Hdo/CdUkqeHQbBLYYRSkG0ViZXNgrSSbgIGV9lyJDJO0gARhC/9i90Ta3AtXfaATLZsNa4fYwZeg65p0OBQ8csZIh7SmfhXfmsR6s57Q+Q24OM15hr+WQf7TsH2e3QQR7Dq9AylDQBHxBNY/DpvfhKbd4Ppv9J8dKoAvDHq2GhoJLIjhvWmYe9Tl6jmQ/Ed97MYxUDQJ5ANudCfs+MQKr8Q5sW5sY+tKO+TMSxkEaS9DnVbOQIoE5A2H3XLtxGiSV+9sHAE/L4BvBuhbrv8Je97DtrUlbpQ2GbY8JU9QuDfx8no5FGh+oVghba58nEs7NADDUVwkW67dcOg4wb1sbf9KyLsfyiXBZLSEJD2luaqvHq3JeAdaP6z/UryUpcm+88HCr6eD/y2BiKVhuA/psbgonOVtEh1JHQepY3Qt6Nh+Xgh5/Z2lRCRYznvZ4n0O2BXOkWJY18cZdBst8fSuXQ0J19ipL0uGcouzdN7K24xtHN4CSzGuO74BbYc5l3FI7mPHB5D3gHvivHkfyFxkPyMF2T2LYd+XcGI7xCdAk9663edaVy0KZN0QK848x4/cc4GlBmCkSnxFWjq/B63udY4VajbkW1A40sUY7wtX5zoHFDwcfx5/VDoL1mmVaKE3fyW+hhSOPFfmFpm8oBz46Q5eg22J4vcWvwLFz9ttSNGiV0hfSUZ5ud5S8R1syIY24yFlsB0w60D0XWRuKJPIX3No1BbS34dGLgf+yXI9ILBhsF0iBcimD8MlGRBfX4+6HM6H3ZPs56jgmzHLBcSZsO6h4KXQ7DUHQwqjc9GmaXdIn+ZeuSBhrI0jYesc8wB4AnF5GuwvME/Duae5iza2MWq0rnrJwpOzoNM/oE6yPh1JmG8aBdv/E9jihdZVM6G1Ubm8WAk2kBDYVS9NCvW3YKJ32VBsupaGb71zfmgJ88TfQWUeHHDNqZr6PIK7bKiBqL8JE7vu6uNtmdiFa99CGNqFa0Mrx678+wDZrwQaIMYenfACoikADaUSe/bEA4imAdRAjD284wZhQABWSWLs6acqIAMGsOpMjD0+poEYFIBV2jn2/F3wAFa5fLEHGE25Mz47xZ4ADR1DjULsEVoLgDSeTZE7V6Oj9niF6zokkgyTiGP6ef0MsuO8Yw9xWyCN2raOPQVvEZJ67jn2ZQTWwQkOX4fRAQV5dUK+DkNKr3x9HcZeoBhV+zqMomh/Hcb/AffJLcCgOMRRAAAAAElFTkSuQmCC'
                  },
                label: {
                    text: '地面站',
                    fontSize: 12,
                    fill: '#fff',
                    refX: 0.5,
                    refY: '100%',
                    refY2: 4,
                    textAnchor: 'middle',
                    textVerticalAnchor: 'top',
                }
              },
              markup: [
                {
                  tagName: 'rect',
                  selector: 'body',
                },
                {
                  tagName: 'image',
                  selector: 'image',
                },
                {
                  tagName: 'text',
                  selector: 'label',
                },
              ],
            },
             {
                          label: '作战力量',
                          data: {
                            type: 'defaultCircle'
                          },
                          shape: 'image',
                          width: 40,
                          height: 40,
                          attrs: {
                            body: {
                              stroke: '#fff'
                            },
                            image: {
                               fill:'#ffffff00',
                                width: 40,
                                height: 40,
                                refX: 0,
                                refY: 0,
                                xlinkHref:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAACvdJREFUeF7tnXlsVNcVh3/3vVk8Y49XtqQ4BJdAcMoSUJNCVgNOmqghURtFapQ2hfSPpImjkqaRKEZ1FNNUSlJaICVSlEW0UiWaVIEqpIltllQsoVAWt4AhgIvBgLENnvGMPdu7zXnD2ONZ3zqeoLkSEoK7nPu9c+8995zz3jCMYtnW4B4TCAi3WkQ+TZKkaYKAaRzCBAbuAucuDuYi8Ri4B4x5OJiHQbogSWgTBKEtFGZtNpt0oKahuHu0psGyOfDGZR2OCmfJIglsgcCwgIPPkPnoK5yBtUocWwXwrT2+vubHVlcO6OtSeWu9wmcciYOzpuX9dwsC/zEYfxRgxRkb6arA3eDsA0liG2pfLfqcgXFd3WVobBrALXUn7PbSCUs4+C/BUWXmJFL2zXCKgb3mv3LhvQfX3uQ3QwbDAdIyLSssfpqBvQjgejOE1tBnJwd//bLX/ZbRy9tQgC0r3A9BwBrOcaOGSZrehDG0Q8LzC1cV/92owQwB2LJiYJLEgmsYsNgowczshwObIVrral92nNE7jm6AW+s93+eQ3uFgpXqFyWZ7Bn6FQXhqQaPrb3rG1QxwYwO3lYX6X2fgdXoEGO22HGztZUvRi481sIAWWTQB3NXAy30hD+0j87UMmoNtdjktrofmN7BetbKpBritwTdRCoc+5RzVagfL5fqM4YggWu6vaXCeVSOnKoCf1LunWRlvAmeVagb52tRlvCPIWe0DjcVtSmVWDJA0LxwO7rpm4UWJMd4hitb5SjVREUDa8wbCnn9ea8s2lZbRcnaIrruU7IkZAdJpWx7ybLuGDgylq3NXr8VVk+l0zgiwqd6z5utuqiglFl+PTJzaRtfz6dqnBUhGsgT+oVYBst3OUSFgzE0WlE0SUVAhwFEmgIkcnAOhAWCgW4K3S0L3iTAunw5BCmV21AhgP0hnbKcESNczsMDBXL9hMAEYV21F5R02lEwUFD+zwV4JO1d7M9anG4sk2maluvalBNhU796U63fbkkoR0x8uQOF45eBiiX2xzov+i1JGiHR3rm0sfjhZxaQAyavCGTZn7Hm0KjBgyqIC3HCXFSzjLp5ayC//4cf/diq7wTGOxcm8OAnDy273opIjueqSEiwM33q0AGNvseh+fJ37gzj60aCifsgV1tPfVx3vT0wA2LzS/QI43lDUa5YriVaGWU84UFYlphw56OXw9UigA8VWmF49r5wOY/+7PsWz4OAv1DaWrI5tMGIEcsPbSsafyiFP8pCsmeAN9Eo4+pFfPl2jpfRGC6ofscswkxXPeQl7/5j5IIlp2xnou1gVGx4YAbBlpedpzvl6xY8kSxUzwfO7Ofas8yI0kGiWWBwMtz9biIKSRG3svyDhizdVAQRj7JmFr7jeGrr5Rf9C0bOWlZ4vRy0ARPFNmiMDeMzBmAkeyd/6lwF0HYloHpk1jnIBpJHRfsZVWzDjh46Ex913Jox9bytfwpEBcGrhK64p0Wjf0GP5bLnnHkHk281Uqhvm2VE6SYBoA0Q7g2hjsNgBwUZ/B3yXOA6850NwMKJJSuCBAzsa+xEKRNrM/pETFVNF9J4I4cCGSHiYxrmnvijhxL54KIT/fKA+hCyF2b33veraEeF5tTSvcL8DhqVmAaxaYMfkGlvK7j2dknp4iNwwdvzGE+mXATW/dkEQAR4Gtr3skW8hVO5e7oLVOXL4Uy0BnN6uIdrJ8e6iVcVPDQEk06W8sPiCWUFvs+BFcex8rR+D7ggp0vJvfNuKc/uCOLMrAsfuYrjzpaKEh7f/7QFcOTN86ChXHu7u9bonkEkja6CZhrPZ8Ej+01sDOLUttSZNvteOqoUjtZ809/Pfekbst8oBgvIdZMNaBti0wrOaMf5zNR0oqZsNeCQHLdd/vz+AK+2J2lQ2yYLZSxzyso4tZ3cH0bZFmRGdbK6cs9/XrnIti2hgvecQB5+pBIrSOtmCF5WH9jqC0vXfIHw9HM4KhnG3WDHxO1b5ZI4tdDrv/oNXPqm1FgZ2eGGjaxajFLNwCF0GZEkNyZJteGohnPtXEMc2a9e+q+Nx0YJx7NNf9deKgvSZWiFS1c91eAEPx+61yY1utQzCknAfa6nvq+Nga9Q2TlY/1+HR0j24YQC9J7WcvIkzZozVseb6vnUAe1YvwIopImY87pCN32RFq52nV67Y9sc2+XFunzL3lbJx+ZsEsAlgi5Q1SF+LLH5yqY+ttmDMVAvEgkj9XIB3stmP9h1GwqOZ8WbWvNJzGFxOtTW0kNlQVmVFeZWI9h1+ddczQyUBTjYHZBkML4y1spaV7tPZcp4qutsaPEvT4EWcH+10iHR/Fb6rMFjuhO6uNXg0wa/CvT2sud5Nup36lm8A2WsR3lUsgawAdFYImPtTJ2xFOiJAKh6kOQdGUgECWVvCheMEzFliPsQswoss4WweImZDzCY8eQ+kQ8QsM4YGsNgZKufZRjgtzYIYD885RoCvW7uzQNGOQWaMkYZ0/KA3Ly6QnZtn9wTR9vHw5d1oiPHwSitFTP1eAfauVxcwUgQtphJjvMmwq1z84GWTLZizxDEUNDALYjJ4s590wterOmSplh845+soEvcc53yt6tZpGpDZcvtzTjk6FluMhpgKnmgH1AbNtcxfdiYY7c4iQSbX2FG1ILlpaRTEdPBIhp62MA7+WWXIUiVF2Z1lhkOVgjhzljpBG3myohdiJng0ZldrCK0b1YcsVTCMOFSpgVEufdd1Ijznw7IMZkFUAo/GV5M4pALaUNUhlz79ixFBpUl32DDlfjtOfBLAmd3D4UQjNVEpPJqT3qBRJqgjg0o68wFleN+1D4159MNBdB4MGqqJauDRwO3bAzjZYoIL6+osR4Q19QTW4+F5uyg9wwt//3Cij97lrBaeFOA4QGHOjsh2YnyJC6zTAFpTO6Y+UIDK+VZZxnh45KEOX81Z0QqxYw85Q4c9yWQkk51HpkqyIsP702DSGLFhIONTO6hjPclFBLF8imWE5jlKIydxx+6Q5j3R6mQI+oY1OSfgAUiaXKQ3vY3uvSF/ZLJReAVlETPmxBblB8uRvw7i/OHI/hlfZj7uxNjpybNTs6J5JFCq9Db6PyMSLOPhUfrZ0U1+dO4fXobJljOl5h7/2I8LrcnhkXzXz7Vh+iOJazdr8GQPTIoESxJQb4qvEnhRrYqFePFwCMe3+BHwpvee2AoF3PlS4YhUjWzCI/MybYovTa6pvm8ZA/ud2g1XCbzyKgvIE0MHAxWCSMZ393Hlge65S50onRxZxlmGh4xJ5iSU1tccaMO/9SdOuK4TkGzZEryZTxTIgffYPVHtg6qcZ8fUB21Zh6f4NQd5L9RoWMsQn3Ti7N7giD0vFh71T7l5lB2Vackmg0uafltdIQ6ZbarEDa74RZtoO62velEqWWySODkUbvuZcyjlIzQIHHjfB/c57UZu4VgB3ksme5tjAKp+1Suihca9bBg1to2Ap3bJ662v+WVDGtjI112/uciOS0dDujRPLwwt7TW/7jq8lPMvXKcDnzHSnX/lP/0HeTICJPr5j06k1kFFAKl5/rMnySEqBkjN8x/eSYSoCmBUE/OffhoGqRpgdE/Mf3wsAlETQPnOnP/8nT6AUSXOf4BRi3ke1yb/CVADIEa9OPmP0OqEKYdIi0qeAccvcujjFZ1geKO3v299Tn8GOZZ9/kPcOjUx2jz/KXiDQMqmT/7HCAykKd+vr/4chhC+mQPTQH+YMD7dz2GASxcBtDGgLSSJx0b75zD+D4XEDQNld6KbAAAAAElFTkSuQmCC'
                              },
                            label: {
                               text: '作战力量',
                               fontSize: 12,
                               fill: '#fff',
                               refX: 0.5,
                               refY: '100%',
                               refY2: 4,
                               textAnchor: 'middle',
                               textVerticalAnchor: 'top',
                            }
                          },
                          markup: [
                            {
                              tagName: 'rect',
                              selector: 'body',
                            },
                            {
                              tagName: 'image',
                              selector: 'image',
                            },
                            {
                              tagName: 'text',
                              selector: 'label',
                            },
                          ],
                        },
  ]
  if (type) {
    const obj = nodeShapeList.find(item => { return item.data.type === type })
    return obj || nodeShapeList
  }
  return nodeShapeList
}

/**
* 节点连接桩设置
* 这里设置了上下左右四个
* 并且给style设置了 visibility: 'hidden'，默认是隐藏的。
* 如果设置了隐藏记得在画布里面设置鼠标经过显示。
* graph.on('node:mouseenter', () => {
        const container = document.getElementById('wrapper')
        const ports = container.querySelectorAll('.x6-port-body')
        for (let i = 0, len = ports.length; i < len; i = i + 1) {
          ports[i].style.visibility = val ? 'visible' : 'hidden'
        }
    })
* 如果需要常显去掉每个链接桩里面的
  style: {
        visibility: 'hidden',
     },
* 就可以了
*/
export const configNodePorts = () => {
  return {
    groups: {
      top: {
        position: 'top',
        attrs: {
          circle: {
            r: 4,
            magnet: true,
            stroke: '#5F95FF',
            strokeWidth: 1,
            fill: '#fff',
            style: {
              visibility: 'hidden',
            },
          },
        },
      },
      right: {
        position: 'right',
        attrs: {
          circle: {
            r: 4,
            magnet: true,
            stroke: '#5F95FF',
            strokeWidth: 1,
            fill: '#fff',
            style: {
              visibility: 'hidden',
            },
          },
        },
      },
      bottom: {
        position: 'bottom',
        attrs: {
          circle: {
            r: 4,
            magnet: true,
            stroke: '#5F95FF',
            strokeWidth: 1,
            fill: '#fff',
            style: {
              visibility: 'hidden',
            },
          },
        },
      },
      left: {
        position: 'left',
        attrs: {
          circle: {
            r: 4,
            magnet: true,
            stroke: '#5F95FF',
            strokeWidth: 1,
            fill: '#fff',
            style: {
              visibility: 'hidden',
            },
          },
        },
      },
    },
    items: [
      {
        group: 'top',
      },
      {
        group: 'right',
      },
      {
        group: 'bottom',
      },
      {
        group: 'left',
      },
    ]
  }
}

// 连线 label 设置
export const configEdgeLabel = (labelText, fontColor, fill, stroke) => {
  if (!labelText) return { attrs: { labelText: { text: '' }, labelBody: { fill: '', stroke: '' } } }
  return {
    markup: [
      {
        tagName: 'rect',
        selector: 'labelBody',
      },
      {
        tagName: 'text',
        selector: 'labelText',
      },
    ],
    attrs: {
      labelText: {
        text: labelText || '',
        fill: fontColor || '#333',
        textAnchor: 'middle',
        textVerticalAnchor: 'middle',
      },
      labelBody: {
        ref: 'labelText',
        refX: -8,
        refY: -5,
        refWidth: '100%',
        refHeight: '100%',
        refWidth2: 16,
        refHeight2: 10,
        stroke: stroke || '#555',
        fill: fill || '#fff',
        strokeWidth: 2,
        rx: 5,
        ry: 5,
      },
    }
  }
}

// 键盘事件
export const graphBindKey = (graph) => {
  graph.bindKey(['meta+c', 'ctrl+c'], () => {
    const cells = graph.getSelectedCells()
    if (cells.length) {
      graph.copy(cells)
    }
    return false
  })
  graph.bindKey(['meta+x', 'ctrl+x'], () => {
    const cells = graph.getSelectedCells()
    if (cells.length) {
      graph.cut(cells)
    }
    return false
  })
  graph.bindKey(['meta+v', 'ctrl+v'], () => {
    if (!graph.isClipboardEmpty()) {
      const cells = graph.paste({ offset: 32 })
      graph.cleanSelection()
      graph.select(cells)
    }
    return false
  })
  //undo redo
  graph.bindKey(['meta+z', 'ctrl+z'], () => {
    if (graph.history.canUndo()) {
      graph.history.undo()
    }
    return false
  })
  graph.bindKey(['meta+shift+z', 'ctrl+shift+z'], () => {
    if (graph.history.canRedo()) {
      graph.history.redo()
    }
    return false
  })
  // select all
  graph.bindKey(['meta+a', 'ctrl+a'], () => {
    const nodes = graph.getNodes()
    if (nodes) {
      graph.select(nodes)
    }
  })
  //delete
  /*
  graph.bindKey('delete', () => {
    const cells = graph.getSelectedCells()
    if (cells.length) {
      graph.removeCells(cells)
    }
  })
  */
  // zoom
  graph.bindKey(['ctrl+1', 'meta+1'], () => {
    const zoom = graph.zoom()
    if (zoom < 1.5) {
      graph.zoom(0.1)
    }
  })
  graph.bindKey(['ctrl+2', 'meta+2'], () => {
    const zoom = graph.zoom()
    if (zoom > 0.5) {
      graph.zoom(-0.1)
    }
  })
  return graph
}
