export const data = [
  {
    "id": "1",
    "shape": "lane",
    "width": 200,
    "height": 500,
    "position": {
      "x": 60,
      "y": 60
    },
    "label": "1"
  },
  {
    "id": "2",
    "shape": "lane",
    "width": 200,
    "height": 500,
    "position": {
      "x": 260,
      "y": 60
    },
    "label": "2"
  },
  {
    "id": "3",
    "shape": "lane",
    "width": 200,
    "height": 500,
    "position": {
      "x": 460,
      "y": 60
    },
    "label": "3"
  },
  {
    "id": "4",
    "shape": "lane",
    "width": 200,
    "height": 500,
    "position": {
      "x": 660,
      "y": 60
    },
    "label": "4"
  },
  {
    "id": "5",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 110,
      "y": 120
    },
    "label": "Start",
    "attrs": {
      "body": {
        "rx": 30,
        "ry": 30
      }
    },
    "parent": "1"
  },
  {
    "id": "6",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 320,
      "y": 120
    },
    "label": "Process",
    "parent": "2"
  },
  {
    "id": "7",
    "shape": "lane-polygon",
    "width": 80,
    "height": 80,
    "position": {
      "x": 520,
      "y": 110
    },
    "label": "Judge",
    "parent": "3"
  },
  {
    "id": "8",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 510,
      "y": 240
    },
    "label": "Process",
    "parent": "3"
  },
  {
    "id": "9",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 720,
      "y": 240
    },
    "label": "Process",
    "parent": "4"
  },
  {
    "id": "10",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 720,
      "y": 350
    },
    "label": "Process",
    "parent": "4"
  },
  {
    "id": "11",
    "shape": "lane-polygon",
    "width": 80,
    "height": 80,
    "position": {
      "x": 520,
      "y": 340
    },
    "label": "Judge",
    "parent": "3"
  },
  {
    "id": "12",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 510,
      "y": 470
    },
    "label": "Process",
    "parent": "3"
  },
  {
    "id": "13",
    "shape": "lane-rect",
    "width": 100,
    "height": 60,
    "position": {
      "x": 300,
      "y": 470
    },
    "label": "End",
    "attrs": {
      "body": {
        "rx": 30,
        "ry": 30
      }
    },
    "parent": "2"
  },
  {
    "id": "14",
    "shape": "lane-edge",
    "source": "5",
    "target": "6"
  },
  {
    "id": "15",
    "shape": "lane-edge",
    "source": "6",
    "target": "7"
  },
  {
    "id": "16",
    "shape": "lane-edge",
    "source": "7",
    "target": "8",
    "label": "Yes"
  },
  {
    "id": "17",
    "shape": "lane-edge",
    "source": "7",
    "target": "9",
    "label": "No"
  },
  {
    "id": "18",
    "shape": "lane-edge",
    "source": "8",
    "target": "9"
  },
  {
    "id": "19",
    "shape": "lane-edge",
    "source": "9",
    "target": "10"
  },
  {
    "id": "20",
    "shape": "lane-edge",
    "source": "10",
    "target": "11"
  },
  {
    "id": "21",
    "shape": "lane-edge",
    "source": "11",
    "target": "12",
    "label": "Yes"
  },
  {
    "id": "22",
    "shape": "lane-edge",
    "source": "11",
    "target": "13",
    "label": "No"
  },
  {
    "id": "23",
    "shape": "lane-edge",
    "source": "12",
    "target": "13"
  }
]

export const circle = [
  {
    "position": { "x": 416, "y": 2057 },
    "size": { "width": 80, "height": 80 },
    "attrs": {
      "text": { "text": "图片" },
      "body": { "stroke": "#00000000", "fill": "#00000000" },
      "image": { "width": 80, "height": 80, "refX": 0, "refY": 0, "xlinkHref": "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAAA8CAYAAAA5S9daAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAzWSURBVGhD3duJcxzFFQbw/EuAMWBjzGFDIMGGcAVCUYGCFMHcxWmgQkJwAKPLlqwDW7d3JeuWdleWjS1sIIBJFaRSRTkQMHJiLOva2dmdvSTr5Xvd06OZnd5DKxmTTNVXQt7VZN5Pr3u6Z5WfWUeC5E2AEu8HKH5YxjwkE+OMIQf3UxSZQ2aRmdFOmol00nSkg6bCHXSOE5KZtKO+V5nihPEznAjOIRJwJUiznNF86dJEvjYnEnASHcX1Igau0xjtIAPXGeOEW8lE4uEWWjaCkRehUyAICLtYHYIACPF78yEERbxFu6MD4MjX3QhRDYIAEAhtSIsOIUDW+0FK2AAeBO4CgYATrxrC/jI6QQegohAkRDEEEwAcfycsA+GH4XY6vnc3De2upP6aSuqrrigxeL8Kfm5ApMqTQU1OtDZqCndneQjcCStCONXfQv1NTfTdt99QKp2h+YXzFzRnJiaoo7qWPm5u0BQv4weQCEZehHxzQhGEGBBmcdL+ut1kmqb2gi9EGOHEB8fpM+Sj5nofgBchqEWI+YZDQYRgQYRT/a10/OCY9mIvVATCsQ8pO79AJ8aPaSFWBwEAuQhxDcJXXe/Rl598qr3YCxWFoL7/7KgXYq5shOZCCDK6Tviqa+9FR+B8fuw4/RVzhATQIxgiuQjty0CwAfwI+34SCJnsPOaIY5gs6yVChMMAbgRXJ+A2XgaC3QkK4CeGoHJifFxC5CJgzWFwGEEAKASObk6wAXwIBzsp2fAGpV55hDLP/IayT/2a4o/dRcbTD1D2tSdkXpXJvPK4SHq7TOplOy/JJF/aRskXt5H1gp3nH6MEZ/vTZL31B0r2dVE2FtMWOn32HI2HRrWvcXiyFEODEUSKIIhOyF0x6hBCbaLo+Xs20sJd14jM34ncsYGyv3Ll9g2Uue1qSiOprTLJW9eLWL9cSuIX6yh+s4z586tEYpybriLjxitFYvffTplT3/mKzCKhQDf9cOas7zWVz8d5jqj3I2BFWhzBBeBGSG9/mLIugAUbwIOwigDRzVfS3KYryHjwHprPZH1FmkaMIl09FAkeoEO9/b6M9Q5Qy9vv0hftTS6APAj4BRdFSAzto/l7r1txB3DxAuAWL0I+gLkbZFIf68c/J40Vaiqlz8zkJB1p2F0YAfsb78ToFN/lQbDaq1wAOb/9EgFyO6BUgNnr15LV0awFKJapyWkbIVgYgTsh5CBw8d4wQmrf254h4AbIAICL9wHcfi1l2xtpPjLgJBteSmaoh1L1lRR/6G6neB3A7HVrKb6nRltksawqQvK9HdohkBcAyez8I5V0nMc9/lCYjNs2aQE45q4KbZHFkg9BAugQeE7Ih9AEhDwAKQ0At36modKusrRj4euTFN16gw9g5trVR8jfCQUQLCAsB0AgoNXVkQm2kPXkQ5R44sGlPPsopTubaTFp2e/C+w5HaO56ILgApi8AgtMJYlJ0IYTyIWCR5EbwDYEtfgCeADP1FXZpRKnqHb5JUM0B5qMP0KKVkG9cmCfjgTsFABfPmdq4lmKrjmBD5CJoO+GwF8HXARoAhZDe40Ko2qG9C0Q3YSJE+yexulOH1VTnAZjaeDnFalaGYBRFaC8BoXEHAORv3+kAzRBQAHz7cyMkGSEPACf22G/tdxKlR0dcAGvp3DVAqP4xENo0CACQCPgKhFI7QK0BchHcQ8B9F+A5ILbtIfud6JrwkARA8Qxw7urVRZAAZSAkgOD89rfI4t0AXLwbgMd/6u3X6fzp70WsN7ZrAdRdINnZYhMQxeuqxBBQABLhXW2RxeJFkBCFEVpLQCgRIN8kmNsBDBB78ne0mMlIgfl5mr17iwTYIAEm168ho2rlCDFsrSUC/7fEMAFhhpFQB+4M7ZTA8tmHkHAj+ABk8Qogcccmsp55hJKvPkPWKzK8LRZ5+WmKc156SsR8EV9fe55SAweIslkJgCM13O/pAAZYKcJRIDCADANIBNOFEGeEcC6C3QVOJzTs8ACo4kXu2EzZwW5aTKXsUso7sl/8jaZu3ODpAM7ZdWsoWrmaCNwFAYoLBAkRx5AoAeFNPcCdm+n8d9/YZZR3MF4y2EFTN6zzdQAD/HDVyhDGG2odBNNJEAgMITES3A1AsIoh6OaALFZ46lhMpyl7dIzSXe1OUsG2pQRkknas9mYyd7xOM7du1g4BBbByhDoU3Y1iOagFkV+xQ0YsG8FCNyQBIRFcALkIAsB+FpC4f6vY/AiARJzij97vWQfk2wyppbBaCBUDOHPlZUDYqS2yWKYnZ+iDhj0otgfFcg6IJIFgiTBCAAD7XQg5AAohDgR3B3CSb7wsAPjIDHT77gLms7+n9Eg/pTHZxZ54pGwAzlxF+QjHG+pRdB+lkGSkF+lBuu10IbIbkuiGggiJeomgAMQ6oPJNmwCzen217zZo7dppv0qU2Pmmbynsvg0qBB3Af65YGcKHDY2UjvRTBkkDgpMCRMruCEZIohskQkdhBDcAx3rhcbtEzOwfH/MNATdC3EZYTgdw8SorQfio4T0ADNkZEBgZgdGDdAODIZaB4CyEsAiKbb2eFmNRWSXmhmQTdmu3bHTmAKvGi5C7FC4E4Eb499pLaXZFCPtQ9AgybGdQIGQwNDLohpSA4G4oiiC3wg4AEsUckKz6i12mPHhLfH56SoQnS3WY7/y55A7IBRAI75aLMAuEFhQbQcIIY3BHyG7ION3ACDwvlIDgBpBzAOaG7g5Uv2iXqz/Mt/5UFsDpy2Vmd76jLbJYJEIbij2IjCIM4e4GHUKeW2QcCApAtxcwn9tG2Y+O0WJ0TuwD3Fk49S1Nb7mxrA4QCGsuwXAob50gETpQ7GGEIbgjGEF1AyPwkLCHQ0QhaBZL8b07ycgDoOYAtQ7w3AXyzAGcUgAmADBx2SU0V1enLbJYJMJ+FHsEOYRwN9hDYhSdMMp3Ce4ETIweBBeAQkj0NNIcDwMbwP1AZKULobwAKJ7z/aWXUGxgQFtksUiEQGGEUfvuUAwhfhB78ftuKQhQSgeUAzCxfh1lpmecwgzToqiZ8MRK6f9OagnhaA4C5gQgpIGQdBCwYhQIOQAKgWO211D0pg0+gJnV7gB7CDDA95djG93b6xSVTGdoatbwZS4W9xSv4kUYQ9ScwOsFRsCCyUawAGBFCq0YuRvGkMAeMh6+h2Zuvpqm0REM4OkApBQAVXwuAE+CE2vX0OkN6+nMffdSfPwDpyD+JHrWMMtE4OHACOruwKtHLKFH1dIZe4eSERDzILalWF2ZQ21kDrbRydY6+vvRccpGjZKTcRL1x4hR1rJ8BcXilhaAUxihE0Wru8PS7VHsIdAFchPFO0kgeNcJ3U5yEeJAYAiBgZwMNtOXn3ymvYjViplIaotXKYzAt0gG4C6Qt8aU2EhhRwkEuaXuRC4gAv+ZXTlxn8PA5KcrXqUwAi+WluYC3kAJANwaE6PymYJCSMhd5FLxuQiJMhCsVFp70aXESqad86wMoRnFy9uiBOiVAJgLGIGfLjFCvDCChCinE+JWSnvRpYSHgDpP+Qi8gdprA/QLAO4C9XRJPWIrEQH5n0VowhzAAH3oAAaQj9m8zxn5QWsJCPExzvIQ0tl5UUDu4qaUpDPzznl4faB7j0r+xZJ8qCIBelHsEoBC4CfOy0RAloFwscMI/HhNAhxAwXIYKAT5yB0IAPi/RuAHrRKgG2GAQgj8yH25CLjPngzyX7T+RBHOzjiP3Ll49dmD/ARKfQolEXixlAhpETCR+BBwQhfCRF8bfYjNie4iLnZOff0v+nRfo/Phi8zSp1C+TlhCwELClWIIBk48WFtL8diP93/6KCUprFHCHftpcpA/gXYD5CK4PoYrF4FzGt0w0NhEp/75DSVdi5yLEf5r96mzZ2msq5f+0bnXLt4NkAeBP5WWCF4AB0EBIKYGgbvh3HCAjjfV0VB1FQ1UVtippMEKmaGKKhoWqaGRil0UqthN4cpailTW2dmDNORJPcKv8/tq8XO7KVS5i0ZEami4qhqpoiFkuKaajuypxTBtR6G4tjCuUQTFq7gRsBlcNQROFP9jnLlQ0A6+H8FrSEzkAMVHeigxgpXbCBYwIazlQ9jVhbCuD2FpywmHEP7Kwb87GaJUeJCSYfxcuA8TWR8uvhdF9KCgA0i3iLoWEVyLFiEMBMQMaxBU6zsRhecskEThOBGi/gIkihPPIVG7+NmRgB18P4x/t2MM42eHgTjMGFjDi2A9zyAjg5TmAIWTAhCHoawQFjuhXlwk7vehHjJDKBoxQiga0By+BhH7GviriiEScGW/SCzUiXTgfO1kjrThmlrzISA+BAmRi6A6IBdBQRg2hAGEGDqCITjcGQlgMIgqWEUWjt84iufCY6JwV/F2oSKua3D/ux6BAXIQhlvpvytFjDlU02XBAAAAAElFTkSuQmCC" }, "label": { "fontSize": 14, "fill": "#fff", "text": "图片" }
    }, "shape": "rect", "id": "ced82ee0-b160-4a08-915e-d673b89a785c",
    "data": { "type": "otherImage" }, "markup": [{ "tagName": "rect", "selector": "body" },
    { "tagName": "image" }, { "tagName": "text", "selector": "label" }], "ports": { "groups": { "top": { "position": "top", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "right": { "position": "right", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "bottom": { "position": "bottom", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "left": { "position": "left", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } } }, "items": [{ "group": "top", "id": "aaf69ac7-9cf6-48d0-bedf-871dbe692a0c" }, { "group": "right", "id": "5612c74c-cf4d-4a86-9d04-f742ba197db7" }, { "group": "bottom", "id": "4f95d83a-2bc2-4e46-879a-38890a06ea7c" }, { "group": "left", "id": "ef72aad8-188f-4d58-9b11-28417f1ec879" }] }, "zIndex": 1
  },
  {
    "position": { "x": 657, "y": 2047 },
    "size": { "width": 80, "height": 80 },
    "attrs": {
      "text": { "text": "圆形" },
      "body": { "fill": "#fff", "stroke": "#333" },
      "label": { "text": "圆形", "fontSize": 16, "fill": "#333" }
    },
    "shape": "circle", "id": "1ec2cdae-f6d0-4cf9-8d64-d17021778ba7", "data": { "type": "defaultCircle" }, "ports": { "groups": { "top": { "position": "top", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "right": { "position": "right", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "bottom": { "position": "bottom", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } }, "left": { "position": "left", "attrs": { "circle": { "r": 4, "magnet": true, "stroke": "#5F95FF", "strokeWidth": 1, "fill": "#fff", "style": { "visibility": "hidden" } } } } }, "items": [{ "group": "top", "id": "8f4956ba-2a68-4e8a-b480-9ce3b64cf9ab" }, { "group": "right", "id": "22c319d8-3058-41a8-8d3f-d4b824fdaf38" }, { "group": "bottom", "id": "2e9da22f-83ab-4802-b2da-90b7a4142a4b" }, { "group": "left", "id": "0a7f50f3-ba88-4a5d-bfff-703bb92d94b0" }] }, "zIndex": 2
  }]


