import Vue from 'vue'
import App from './App.vue'
import API from './api/axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from '@/router/index.js'
Vue.config.productionTip = false
import SuperFlow from 'vue-super-flow'
import 'vue-super-flow/lib/index.css'
import Antd from "ant-design-vue"
import "ant-design-vue/dist/antd.css"
import store from './store'
import '@/utils/css/diacss.less' // global css
import '@/utils/css/edit.less' // global css
import '@/utils/css/container.less' // global css
import '@/utils/css/strength.less' // global css
import '@/utils/css/select.less' // global css
import '@/utils/css/card.less' // global css
import '@/utils/css/table.less' // global css
import '@/utils/css/colorPicker.less' // global css
//内部封装插件直接挂载在vue实例化对象上
import plugins from './plugins' // plugins
import * as echarts from "echarts"
Vue.prototype.$echarts = echarts
Vue.use(Antd)
Vue.use(SuperFlow)
Vue.use(plugins)
Vue.prototype.$API = API
Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

Vue.use(ElementUI)
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
