// import request from '@/utils/request'
//
// export function login () {
//   return new Promise((resolve, reject) => {
//     resolve({ access_token: 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VyX2tleSI6IjdjZDZmMmYzLTI1MWQtNGUwMC1iMjJkLTk2MjliNjFkY2QyZCIsInVzZXJuYW1lIjoiYWRtaW4ifQ.aSqgPgBelhtqRSofAV7Buz6U3xTblSFwNPUqnNg0G6zbDEtQber88dlI4mBKXUiaZ44qXqCv1ddDFsDo1q7yXQ', expires_in: 10000 })
//   })
// }
//
// // 注册方法
// export function register (data) {
//   return request({
//     url: '/auth/register',
//     headers: {
//       isToken: false
//     },
//     method: 'post',
//     data: data
//   })
// }
//
// // 刷新方法
// export function refreshToken () {
//   return request({
//     url: '/auth/refresh',
//     method: 'post'
//   })
// }
//
// // 获取用户详细信息
// export function getInfo () {
//   return request({
//     url: '/system/user/getInfo',
//     method: 'get'
//   })
// }
//
// // 退出方法
// export function logout () {
//   return request({
//     url: '/auth/logout',
//     method: 'delete'
//   })
// }
//
// // 获取验证码
// export function getCodeImg () {
//   return request({
//     url: '/code',
//     headers: {
//       isToken: false
//     },
//     method: 'get',
//     timeout: 20000
//   })
// }
