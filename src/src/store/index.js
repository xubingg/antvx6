import Vue from 'vue'
import Vuex from 'vuex'
import test from './modules/test'
// import getters from './getters'
import createPersistedState from 'vuex-persistedstate';
Vue.use(Vuex)
// console.log(Vuex)
const store = new Vuex.Store({
  modules: {
    test
  },
plugins: [createPersistedState()],
})

export default store
