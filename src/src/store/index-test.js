import Vue from 'vue'
import Vuex from 'vuex'
import test from './modules/test'
// import getters from './getters'

Vue.use(Vuex)
console.log(Vuex)
const ADD = 'ADD'  //商品添加
const store = new Vuex.Store({
  modules: {
    test
  },
  state:{
    goods:[
      {
        id:'0',
        name:'屏幕',
        hint:'照亮你的美',
        price:345,
        num:0,
        img:'1'
      },
      {
         id:'1',
         name:'屏幕1',
         hint:'照亮你的美1',
         price:3451,
         num:1,
         img:'2'
        }
    ]
  },
//   计算属性
   getters:{
    goodsObj: state=>{
        return state.goods
    },
//     goodsById:state=>(id)=>{
//         return state.goods.fillter(item=>item.id===id)
//     }
        goodsById:state=>{
            return (id)=>{
                return state.goods.fillter(item=>item.id===id)
            }
        }
   },
//    方法
   mutations:{
    [ADD](state,{index}){
        state.goods[index].num++
        state.totalNum++
        state.totalPrice+=state.goods[index].price
    }
   },
   actions:{
    increment({commit},payload){
        commit(ADD,payload)
    }
   }
})

export default store
