import Vue from 'vue'
import dgraphService from "@/utils/dgraphService"
const state = {
  type: null,
  shape: null,
  node: [],//存储节点的属性
  edge: [],//存储节点的属性
  jsonArr:[],//画布数据
//   关系管理
  relationData:[],
//   通信网络
  netWork:[],
//   配置节点的子节点
    targetNodeList:[]
}
//this.$store.commit
const mutations = {
// 新增关系
ADD_RELATION:(state,data)=>{
    state.relationData.push(data)
},
EDIT_RELATION:(state,{num,formData})=>{
    console.log(formData,'编辑打印')
    const existingNodeIndex = state.relationData.findIndex(node => node.number === num)
        // 如果找到了节点
        if (existingNodeIndex !== -1) {
          state.relationData.splice(existingNodeIndex, 1, formData)
          console.log(state.relationData,'已更新')
        }
},
// 删除关系
DEL_RELATION:(state,num)=>{
 // 查找要删除的节点的索引
    const index = state.relationData.findIndex(item => item.number === num);
    // 如果找到了相应的节点，将其从数组中移除
    if (index !== -1) {
      state.relationData.splice(index, 1);
    }
},

//   存储画布数据
SET_PANNING_DATA:(state,data)=>{
    state.jsonArr=data
},
// 存储通信网络
SET_NETWORK_DATA:(state,data)=>{
    state.netWork=data
},
// 存储配置节点的子节点
ADD_TARGETNODE_DATA:(state,data)=>{
    state.targetNodeList=data
    console.log(state.targetNodeList,'配置节点的子节点新增了！')
}


}
//this.$store.dispatch
const actions = {
addRelation({commit},data){
    commit('ADD_RELATION',data)
},
editRelation({commit},{num,formData}){
    commit('EDIT_RELATION',{num,formData})
},

delRelation({commit},num){
    commit('DEL_RELATION',num)
},

  setPanningData({commit},data){
    commit('SET_PANNING_DATA',data)
  },
  setNetWorkData({commit},data){
    commit('SET_NETWORK_DATA',data)
  },
  addTargetNode({commit},data){
    commit('ADD_TARGETNODE_DATA',data)
  },

}


export default {
  // namespaced: true,
  state,
  mutations,
  actions
}
