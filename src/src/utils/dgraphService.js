// 导入 Dgraph 库dgraphService.js
const dgraph = require('dgraph-js-http')

// Dgraph 服务器地址
const dgraphEndpoint = '/graphql'

// 创建 Dgraph 客户端
const clientStub = new dgraph.DgraphClientStub(dgraphEndpoint)
const client = new dgraph.DgraphClient(clientStub)

// 导出 Dgraph 服务模块
module.exports = {
  // 执行查询的方法
  query: async function (query) {
    // console.log(dgraph)

    const txn = client.newTxn()
    try {
      const response = await txn.query(query)
      // console.log(response)
      return response

    } catch (error) {
      console.error('Error executing Dgraph query:', error)
      throw error
    } finally {
      await txn.discard()
    }
  },

  // 执行变更的方法
  mutate: async function (mutation) {
    // const request = new dgraph.Request()
    // console.log(request)
    // request.setMutationsList([mutation])

    try {
      await client.newTxn().mutate({ setJson: mutation, commitNow: true })
      console.log('Mutation successful.')
    } catch (error) {
      console.error('Error executing Dgraph mutation:', error)
      throw error
    }
  },

 // 新增数据的方法
  insert: async function (data) {
    try {
      // Create a new transaction and perform the insert mutation
      await client.newTxn().mutate({ setJson: data, commitNow: true })
      console.log('Insertion successful.')
    } catch (error) {
      console.error('Error executing Dgraph insert:', error)
      throw error
    }
  },

  // 删除数据的方法
  remove: async function (uid) {
    try {
      // Create a new transaction and perform the delete mutation
      await client.newTxn().mutate({ deleteJson: { uid }, commitNow: true })
      console.log('Deletion successful.')
    } catch (error) {
      console.error('Error executing Dgraph delete:', error)
      throw error
    }
  },

  // 修改数据的方法
  update: async function (uid, updateData) {
    try {
      // Create a new transaction and perform the update mutation
      await client.newTxn().mutate({ updateJson: { uid, ...updateData }, commitNow: true })
      console.log('Update successful.')
    } catch (error) {
      console.error('Error executing Dgraph update:', error)
      throw error
    }
  },

  // 关闭 Dgraph 客户端连接的方法
  close: function () {
    // console.log(client)
    // client.discard()
    // console.log('Dgraph client connection closed.')
  },
}
