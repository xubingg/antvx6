/*
 * @Author: liqiyuan L15935268112@163.com
 * @Date: 2023-11-17 15:13:35
 * @LastEditors: liqiyuan L15935268112@163.com
 * @LastEditTime: 2023-11-17 15:28:07
 * @FilePath: \jsmind\jsmind_demo\src\router\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/nav',

    },
    {
      path: '/index',
      redirect: '/nav',
      component: () => import('@/views/home'),
      children: [
        { path: '/act', name: 'act', component: () => import('@/views/construct/actChart') },
        { path: '/conduct', name: 'conduct', component: () => import('@/views/construct/conductChart') },
        { path: '/networkChart', component: () => import('@/views/construct/networkChart') },
        { path: '/quest', component: () => import('@/views/construct/questChart') },
        { path: '/strength', component: () => import('@/views/construct/strengthChart') },
        { path: '/analysis', component: () => import('@/views/construct/analysis') },
        { path: '/zhbz', component: () => import('@/views/construct/modules/zhbz') },
        { path: '/nav', component: () => import('@/views/nav.vue') },
        { path: '/relationshipManagement', component: () => import('@/views/construct/relationshipManagement.vue') }
      ]
    },
    { path: '/test1', component: () => import('@/views/Test1') },
    { path: '/test2', component: () => import('@/views/Test2') },
    { path: '/test3', component: () => import('@/views/Test3') },
    { path: '/test4', component: () => import('@/views/Test4') },
    { path: '/flow', component: () => import('@/views/liteGra') },
    { path: '/antvDemo', component: () => import('@/views/avtvDemo') },
    { path: '/fruchterman', component: () => import('@/views/Fruchterman') },

    { path: '/tree', component: () => import('@/views/Tree') },
    { path: '/network', component: () => import('@/views/network') },
    { path: '/swim', component: () => import('@/views/Swim') },
    { path: '/treeDraw', component: () => import('@/views/TreeDraw') },
    { path: '/yongdao', component: () => import('@/views/yongdao') },
    { path: '/yongdao1', component: () => import('@/views/yongdao1') },
    // { path: '/treeConfig', component: () => import('@/views/treeConfig') }
    { path: '/analysis', component: () => import('@/views/construct/analysis') },
    { path: '/graph', component: () => import('@/views/testGraph') },
    { path: '/map', component: () => import('@/views/wrapper') }
  ]
})


