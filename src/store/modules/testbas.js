import Vue from 'vue'
import dgraphService from "@/utils/dgraphService"
import { CommandNode, CombatForce } from '@/modelClass/ysModel'
const state = {
  commandNode: [],//指挥节点
  forceList: [], // 力量节点
  type: null,
  shape: null,
  node: [],//存储节点的属性
  edge: [],//存储节点的属性
  jsonArr:[],//画布数据
//   关系管理
  relationData:[],
}
//this.$store.commit
const mutations = {
// 新增关系
ADD_RELATION:(state,data)=>{
    state.relationData.push(data)
},
EDIT_RELATION:(state,{num,formData})=>{
    console.log(formData,'编辑打印')
    const existingNodeIndex = state.relationData.findIndex(node => node.number === num)
        // 如果找到了节点
        if (existingNodeIndex !== -1) {
          state.relationData.splice(existingNodeIndex, 1, formData)
          console.log(state.relationData,'已更新')
        }
},
// 删除关系
DEL_RELATION:(state,num)=>{
 // 查找要删除的节点的索引
    const index = state.relationData.findIndex(item => item.number === num);
    // 如果找到了相应的节点，将其从数组中移除
    if (index !== -1) {
      state.relationData.splice(index, 1);
    }
},




// 存储作战力量
  SET_FORCE_NODE: (state, force) => {
    state.forceList.push(force)
  },
// 存储指挥节点
  SET_COMMAND_NODE: (state, commandNode) => {
    state.commandNode.push(commandNode)
    console.log(commandNode,'打印存储指挥节点')
  },
  CLEAR_COMMAND_NODE:(state)=>{
    state.commandNode=[]
  },
// 更新指挥节点
// 更新指挥节点
  UPDATE_COMMAND_NODE: (state, { existingCommandNodeId, commandNodeInstance }) => {
//     console.log(commandNodeInstance, 'updatedData::::::')
    // 找到要更新的节点在数组中的索引
    const existingNodeIndex = state.commandNode.findIndex(node => node.id === existingCommandNodeId)
    //           console.log(existingNodeIndex,'existingNodeIndex判断')
    // 如果找到了节点
    if (existingNodeIndex !== -1) {
      state.commandNode.splice(existingNodeIndex, 1, commandNodeInstance)
//       console.log(state.commandNode, '更新后的数据')
    }
  },
// 更新作战力量
  UPDATE_FORCE_NODE: (state, { existingcombatForcNodeId, combatForceInstance }) => {
  console.log(existingcombatForcNodeId,'打印更新作战力量的id')
  console.log(combatForceInstance,'打印更新作战力量的combatForceInstance')
    // 找到要更新的节点在数组中的索引
    const existingNodeIndex = state.forceList.findIndex(node => node.id === existingcombatForcNodeId)
    // 如果找到了节点
    if (existingNodeIndex !== -1) {
      state.forceList.splice(existingNodeIndex, 1, combatForceInstance)
      console.log(state.forceList,'已更新')
    }
  },
// 存储边
  ADD_EDGE_ARR:(state,item)=>{
      state.edge.push(item)
  },
//   add:画布的属性
  ADD_DRAPH_ARR:(state,item)=>{
      state.node.push(item)
  },
//   update:画布的属性
  UPDATE_NODE: (state, { index, updatedNode }) => {
    // 更新现有节点
    state.node.splice(index, 1, updatedNode);
  },
  // delete:画布的属性
  REMOVE_NODE: (state, nodeIdToDelete) => {
    // 查找要删除的节点的索引
    const index = state.node.findIndex(node => node.id === nodeIdToDelete);
    // 如果找到了相应的节点，将其从数组中移除
    if (index !== -1) {
      state.node.splice(index, 1);
    }
  },

//   存储画布数据
SET_PANNING_DATA:(state,data)=>{
    state.jsonArr=data
}



}
//this.$store.dispatch
const actions = {
addRelation({commit},data){
    commit('ADD_RELATION',data)
},
editRelation({commit},{num,formData}){
    commit('EDIT_RELATION',{num,formData})
},

delRelation({commit},num){
    commit('DEL_RELATION',num)
},


// 存储指挥节点
  setCommandNode ({ commit }, commandNode) {
    commit('SET_COMMAND_NODE', commandNode)
  },
// 存储作战力量
  setForce ({ commit }, force) {
    commit('SET_FORCE_NODE', force)
  },
// 更新指挥节点
  updateCommandNode ({ commit }, { existingCommandNodeId, commandNodeInstance }) {
    commit('UPDATE_COMMAND_NODE', { existingCommandNodeId, commandNodeInstance })
  },
// 更新作战力量
  updateForceNode ({ commit }, { existingForceNodeId, commandNodeInstance }) {
    commit('UPDATE_FORCE_NODE', { existingForceNodeId, commandNodeInstance })
  },
  addEdges({commit},item){
    commit('ADD_EDGE_ARR',item)
    },
  // 新数据 添加临时id '_:id'  老数据修改不要
  async setServeCommandNode ({ commit }, commandNode, forceList) {
//     console.log(forceList, '1123')
    let arrcommandNode = []

    commandNode.forEach(item => {
    console.log(item.name,'???')
       let temporaryId = '_:id'
//        console.log(item.isNew)
       let powerARR = []
       if (!item.isNew) {
         temporaryId = ''
       }
      if (item.isNew === undefined) {
        temporaryId = '_:id'
      }
      // item.combatForces.forEach(items => {
      //   powerARR.push({
      //     "Force.name": temporaryId + items.name,
      //     "Force.superiorCommandNode": {
      //       "uid": items.superiorCommand
      //     }
      //   })
      // })
//       console.log(item.graphType)
      arrcommandNode.push({
        "uid": temporaryId + item.id,
        "CommandCenter.name": item.name,
        "CommandCenter.force": powerARR,
        "CommandCenter.graphType": item.graphType
      })
    })
    const addJson = {
      "set": arrcommandNode
    }
    try {
      await dgraphService.mutate(addJson)
      console.log("Data added successfully!")
    } catch (error) {
//       console.error("Error adding data:", error)
    }
  },

  async getServeCommandNode ({ commit }) {
    const query = `
    query{
      queryCommandCenter(func: has(CommandCenter.name)) {
          uid
          CommandCenter.name
          CommandCenter.describe
          CommandCenter.graphType
          CommandCenter.commandNodeDuty
          CommandCenter.duty
          CommandCenter.graphInfo
      CommandCenter.entityProject {
        uid
        EntityProject.name
          },
      CommandCenter.superiorCommandNode {
        uid
        CommandCenter.name
          }
      CommandCenter.belowCommandNode {
        uid
        CommandCenter.name
          }
          CommandCenter.force {
              uid
              Force.name
          Force.superiorCommandNode{
          uid
          CommandCenter.name
        }
          }
          CommandCenter.commandSeat
      }
  }`
    const result = await dgraphService.query(query)
//     console.log('Query Result:', result)
    const res = result.data.queryCommandCenter
    console.log(res,'res')
    res.forEach(item => {
      if (item['CommandCenter.graphType'] === 'defaultYSquare') {
        const commandNodeInstance = new CommandNode()
        commandNodeInstance.setId(item.uid)
        commandNodeInstance.setName(item['CommandCenter.name'])
        commandNodeInstance.setLocation({ position: { x: 400, y: 180 } })
        commandNodeInstance.setIndex(1)
        commandNodeInstance.setText(item['CommandCenter.name'])
        commandNodeInstance.setIsNew(false)
        commandNodeInstance.setGroupType(item['CommandCenter.graphType'])
        commandNodeInstance.addCombatForce('')
        commit('CLEAR_COMMAND_NODE')//清空state.commandNode=[]
        commit('SET_COMMAND_NODE', commandNodeInstance)
      }
      if (item['CommandCenter.graphType'] === 'defaultCircle') {
        const combatForce = new CombatForce()
        combatForce.setId(item.uid)
        combatForce.setName(item['Force.name'])
        combatForce.setDescription({ position: { x: 250, y: 390 } })
        combatForce.setSuperiorCommand(item.id)
        combatForce.setGroupType(item['CommandCenter.graphType'])
        commandNodeInstance.setIsNew(false)
        commit('SET_FORCE_NODE', combatForce)
      }
    })
  },

//   存储画布的属性
//   addDraphArr({commit},item){
//     commit('ADD_DRAPH_ARR',item)
//   },
//   更新画布的属性

  updateNode({ commit, state }, updatedNode) {
    const index = state.node.findIndex(node => node.id === updatedNode.id);
//     未找到返回-1
    if (index !== -1) {
    console.log('如果找到了相应的节点，更新它')
      // 如果找到了相应的节点，更新它
      commit('UPDATE_NODE', { index, updatedNode });
    } else {
        console.log('如果未找到，添加新的节点')
      // 如果未找到，添加新的节点
      commit('ADD_DRAPH_ARR', updatedNode);
    }
  },
  setPanningData({commit},data){
    commit('SET_PANNING_DATA',data)
  }
}


export default {
  // namespaced: true,
  state,
  mutations,
  actions
}
