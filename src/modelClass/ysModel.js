// import { extend } from "vue/types/umd"

/**
 * 实体节点 {
    名称: String
    描述: String
    位置: String
    通信装备: [通信设备]
    命令接口()
}
 */
export class EntityNode {
  constructor(name, description, location) {
    this.name = name
    this.description = description
    this.location = location
    this.communicationsEquipment = [] // 存储通信设备对象的数组
  }



  // 添加通信设备
  addCommunicationEquipment (device) {
    this.communicationsEquipment.push(device)
  }

  // 定义命令接口方法
  commandInterface () {
    console.log('执行命令接口...')
  }
}

/**
 * 指挥节点 {
    名称: String
    描述: String
    位置: String
    通信装备: [通信设备]
    指挥节点职责: String
    作战力量: [作战力量]
}
 */
export class CommandNode {
  // id, name, description, location, index, ports, groupPosition, responsibilities, text
  constructor() {
    this.id = ''
    this.text = ''
    this.index = ''
    this.ports = {} // 不需要
    this.name = ''
    this.graphType = ''
    this.groupPosition = ''
    this.description = ''
    this.location = ''
    this.isNew = ''
    this.communicationsEquipment = [] // 存储通信设备对象的数组
    this.responsibilities = ''
    this.combatForces = [] // 存储作战力量对象的数组
  }

  setIsNew (isNew) {
    this.isNew = isNew
  }

  setDescription (description) {
    this.description = description
  }

  setGroupType (graphType) {
    this.graphType = graphType
  }

  setPorts (ports) {
    this.ports = ports
  }

  setId (id) {
    this.id = id
  }
  setName (name) {
    this.name = name
  }
  setIndex (index) {
    this.index = index
  }

  setText (text) {
    this.text = text
  }

  setGroupPosition (groupPosition) {
    this.groupPosition = groupPosition
  }

  setLocation (location) {
    this.location = location
  }

  setResponsibilities (responsibilities) {
    this.responsibilities = responsibilities
  }

  // 添加通信设备
  addCommunicationEquipment (device) {
    this.communicationsEquipment.push(device)
  }

  // 添加作战力量
  addCombatForce (force) {
    this.combatForces.push(force)
  }
}


/**
 * 指挥席位  {
    名称: String
    描述: String
    位置: String
    通信装备: [通信设备]
    指挥节点职责: String
    作战力量: [作战力量]
}
 */

export class CommandSeat extends CommandNode {
  constructor(name, description, location, responsibilities, seatType) {
    // 调用父类的构造函数
    super(name, description, location, responsibilities)


  }
}

/**
 * 指挥所 extends 指挥节点 {

    名称: String
    描述: String
    位置: String
    通信装备: [通信设备]
    指挥节点职责: String
    作战力量: [作战力量]
    职责: String
    指挥席位: [指挥席位!]
}
 */

export class CommandCenter extends CommandNode {
  constructor(name, description, location, responsibilities, duty) {
    // 调用父类的构造函数
    super(name, description, location, responsibilities)

    // 新增属性
    this.duty = duty
    this.commandSeats = [] // 存储指挥席位对象的数组
  }

  // 新增方法
  addCommandSeat (commandSeat) {
    this.commandSeats.push(commandSeat)
  }
}

/**
 * 作战力量 extends 实体节点 {
    名称: String
    描述: String
    位置: String
    通信装备: [通信设备]
    作战力量类型: String
    上级指挥: 指挥节点!
}
 */
export class CombatForce {
  constructor() {
    this.id = ''
    this.name = ''
    this.description = ''
    this.location = {}
    this.forceType = ''
    this.graphType = ''
    this.isNew = ''
    this.superiorCommand = '' // 上级指挥，关联到指挥节点对象
  }

  setIsNew (isNew) {
    this.isNew = isNew
  }

  setGroupType (graphType) {
    this.graphType = graphType
  }

  setId (id) {
    this.id = id
  }
  setName (name) {
    this.name = name
  }
  setDescription (description) {
    this.description = description
  }

  setForceType (forceType) {
    this.forceType = forceType
  }
  setSuperiorCommand (superiorCommand) {
    this.superiorCommand = superiorCommand
  }


  // 新增方法
  getSuperiorCommand () {
    return this.superiorCommand
  }
}
