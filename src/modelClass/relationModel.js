/**
 * 关系管理 {
    关系编号: String
    关系名称: String
    关系样式: String
    关系描述: String
}
 */
export class RelationManage{
    constructor(relationCode,relationName,sideStyle,sideColor,relationDescribe){
        this.relationCode = relationCode
        this.relationName = relationName
        this.sideStyle = sideStyle
        this.sideColor = sideColor
        this.relationDescribe = relationDescribe
    }
}
