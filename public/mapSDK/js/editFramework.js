
//标绘控制器
function createFramework (mapSDK, token) {

  var thisObject = {};
  thisObject.extension = {};
  thisObject.mapSDK = mapSDK;
  // var plotExtension = new QTMap.QTPlotExtension(mapSDK);
  // plotExtension.init();

  // var plotComplexExtension = new QTMap.QTPlotComplexExtension(mapSDK);
  // plotComplexExtension.init();


  //选择集变化事件添加
  thisObject.mapSDK._selectSet.EventSelectSetChanged.addEventHandler(function (sender) {

    thisObject.onSelectSetChanged();
  }, thisObject);

  //选择发生变化
  thisObject.onSelectSetChanged = function (sender, host, args) {
    // thisObject.mPropertyGridView.update(thisObject.mapSDK._selectSet.getObject(0));
  }
  return thisObject;
}
var QTMapEditUtils = {

  countryColorDict: {//国家与颜色对照
    '中国': {
      color: 'rgb(210,0,0)',
      gradient: 'linear-gradient(90deg, rgba(210, 0, 0, 0), rgba(210, 0, 0, 0.5))',
      text: '#000',
      abbreviation: '中'
    },
    '俄罗斯': {
      color: 'rgb(200, 0, 167)',
      gradient: 'linear-gradient(90deg, rgba(200, 0, 167, 0), rgba(200, 0, 167, 0.5))',
      text: '#000',
      abbreviation: '俄'
    },
    '朝鲜': {
      color: 'rgba(200, 124, 0, 1)',
      gradient: 'linear-gradient(90deg, rgba(200, 124, 0, 0), rgba(200, 124, 0, 0.5))',
      text: '#000',
      abbreviation: '朝'
    },
    '美国': {
      color: 'rgb(0, 0, 220)',
      gradient: 'linear-gradient(90deg, rgba(0, 0, 220, 0), rgba(0, 0, 220, 0.5))',
      text: 'rgb(0, 0, 220)',
      abbreviation: '美'
    },
    '印度': {
      color: 'rgb(0, 112, 192)',
      gradient: 'linear-gradient(90deg, rgba(0, 112, 192, 0), rgba(0, 112, 192, 0.5))',
      text: 'rgb(0, 112, 192)',
      abbreviation: '印'
    },
    '日本': {
      color: 'rgb(84, 130, 53)',
      gradient: 'linear-gradient(90deg, rgba(84, 130, 53, 0), rgba(84, 130, 53, 0.5))',
      text: 'rgb(84, 130, 53)',
      abbreviation: '日'
    },
    '英国': {
      color: 'rgb(93, 0, 230)',
      gradient: 'linear-gradient(90deg, rgba(93, 0, 230, 0), rgba(93, 0, 230, 0.5))',
      text: 'rgb(93, 0, 230)',
      abbreviation: '英'
    },
    '澳大利亚': {
      color: 'rgb(93, 0, 230)',
      gradient: 'linear-gradient(90deg, rgba(93, 0, 230, 0), rgba(93, 0, 230, 0.5))',
      text: 'rgb(93, 0, 230)',
      abbreviation: '澳'
    },
    '加拿大': {
      color: 'rgb(93, 0, 230)',
      gradient: 'linear-gradient(90deg, rgba(93, 0, 230, 0), rgba(93, 0, 230, 0.5))',
      text: 'rgb(93, 0, 230)',
      abbreviation: '加'
    },
    '越南': {
      color: 'rgb(128, 119, 0)',
      gradient: 'linear-gradient(90deg, rgba(128, 119, 0, 0), rgba(128, 119, 0, 0.5))',
      text: 'rgb(128, 119, 0)',
      abbreviation: '越'
    },
    '蒙古': {
      color: 'rgb(118, 113, 113)',
      gradient: 'linear-gradient(90deg, rgba(118, 113, 113, 0), rgba(118, 113, 113, 0.5))',
      text: 'rgb(118, 113, 113)',
      abbreviation: '蒙'
    },

  },
  panelItems: {

  },
  // '边线与填色', '文字注记', '标号控制'
  StylePanelDict: {
    "QTAttributeStyle": "性能参数",
    "QTLabelStyle": "文字注记",
    "QTSymbolStyle": '标号控制',
    "QTArrowSymbolStyle": '标号控制',
    'QTLineStyle': '线',
    "QTFillStyle": '面',
    'QTOutLineStyle': '边线',
    'QTFlagStyle': '旗帜',
    'QTGradientFillStyle': '面',
    'QTArrowOutlineMultiSegmentStyle': '边线'
  },

  /**
   * sdk中对象的字段名与UI上的字段的映射关系
   */
  sdkItemToUIItem: {
    "QTLabelStyle": ["labelTextAttribute"],
    // "QTSymbolStyle": ["matchingLabelAttribute", "showZoomAttribute", "sizeRotateAttribute", "offsetAttribute", "areaAttribute"],
    // "QTFlagStyle": ["multiFlagContainerTextAttribute", "areaAttribute"],
    "QTLineStyle": ["lineAttribute", "labelTextAttributeCustomLine"],
    "QTArrowOutlineMultiSegmentStyle": ["lineAttribute", "labelTextAttributeCustomLine"],
    "QTLineArrowSymbolStyle": ["areaAttributeCustomFill", "sizeRotateAttribute"],
    "QTSymbolStyle": ["sizeRotateAttribute", "offsetAttribute", "areaAttribute"],//, "showZoomAttribute","matchingLabelAttribute",
    "QTFlagStyle": ["multiFlagContainerTextAttribute", "areaAttributeCustomFlag", 'lineAttributeCustomFlag'],
    "QTFillStyle": ["areaAttributeCustomFill", "labelTextAttributeCustomFill"],
    'QTGradientFillStyle': ["areaAttributeCustomFill", "labelTextAttributeCustomFill"],
    "QTOutlineStyle": ["lineAttribute"],
    "QTArrowOutlineStyle": ["lineAttribute"],

    // 'QTCenterCircleStyle': ['lineAttributeCustomCenterCircle', 'lineAttributeCustomPolyline'],
    // 'QTTrianglePolyLineStyle': ['lineAttributeCustomPolyline', 'areaAttributeCustomFill'],
    // 'QTClosePolyLineStyle': ['lineAttributeCustomPolyline', 'areaAttributeCustomFill', 'sizeRotateAttribute', 'offsetAttribute', 'areaAttribute']
  },
  /**
   * ui的item和标号的字段的对应关系
   */
  itemToPlotAttri: {
    "labelTextAttribute": {//注记
      "outTextContent": "text",
      "contentTextIndex": "",
      "indexFontType": "label-type",
      "outTextFontSize": "label-size",
      "outTextFontType": "label-type",
      "contentTextColor": "label-color",
      "indexTextIsBold": "bold",
      "indexTextIsItalics": "",
      "outTextIsStroke": "",
      "outTextStrokeColor": "label-halo-color",
      "outTextStrokeWidth": "label-halo-width",
      // "outTextPosition": "label-anchor"
    },
    "labelTextAttributeCustomFill": {
      "outTextContent": "text",
      "contentTextIndex": "",
      "indexFontType": "label-type",
      "outTextFontSize": "label-size",
      "contentTextColor": "label-color",
      "indexTextIsBold": "bold",
      "indexTextIsItalics": "",
      "outTextIsStroke": "",
      "outTextStrokeColor": "label-halo-color",
      "outTextStrokeWidth": "label-halo-width",
      "outTextPosition": "label-anchor",
      "outTextSpace": 'label-spacing'
    },
    "labelTextAttributeCustomLine": {
      "outTextContent": "text",
      "contentTextIndex": "",
      "indexFontType": "label-type",
      "outTextFontSize": "label-size",
      "contentTextColor": "label-color",
      "indexTextIsBold": "bold",
      "indexTextIsItalics": "",
      "outTextIsStroke": "",
      "outTextStrokeColor": "label-halo-color",
      "outTextStrokeWidth": "label-halo-width",
      "outTextPosition": "label-anchor",
      "outTextSpace": 'label-spacing'
    },
    "matchingLabelAttribute": {//搭配标号

    },
    "showZoomAttribute": {//层级控制

    },
    "sizeRotateAttribute": {//大小和角度
      "gradeWidth": "width",
      "gradeHeight": "height",
      "gradeRotate": "rotation"
    },
    "offsetAttribute": {//偏移量

    },
    //多旗标号内部文字属性
    "multiFlagContainerTextAttribute": {
      // multiFlagFontType: "黑体",
      multiFlagLabelTableData: [
        {
          text: 'text',
          type: '1'
        }
      ],
      multiFlagTextBold: "fontBold",
      multiFlagTextColor: "fontColor",
      multiFlagTextItalics: "fontItalices",
      multiFlagTextSize: "fontSize"

    },
    //填充属性
    "areaAttribute": {
      "areaColor": 'color',
      "colorTransparent": 'opacity',
      // 'areaColorType': '',先注释掉，否则会赋值为undefined
      'areaGradientColor': 'color'
    },

    // 箭头的填充属性
    "arrowAttributeCustomFill": {
      "areaColor": 'color',
      "colorTransparent": 'fill-opacity',
      // 'areaColorType': '',
      'areaGradientColor': ''//注意箭头的填充颜色字段是color
    },
    // 面的填充属性
    "areaAttributeCustomFill": {
      "areaColor": 'fill-color',
      "colorTransparent": 'fill-opacity',
      // 'areaColorType': '',
      'areaGradientColor': 'fill-color'//注意箭头的填充颜色字段是color
    },
    // 旗帜的填充属性
    "areaAttributeCustomFlag": {
      "areaColor": 'fillColor',
      "colorTransparent": 'opacity',
      // 'areaColorType': '',
      'areaGradientColor': ''
    },
    // 边线属性
    "lineAttribute": {
      "colorTransparent": 'line-opacity', // 透明度
      // "lineAfterArrow": '1', // 箭头头部（暂时写死）
      // "lineArrowSize": '1' // 箭头大小（暂时写死）
      "lineColor": 'line-color',
      'lineStyle': 'line-dasharray',
      "lineWidth": 'line-width',
      // 'lineStyle': 'line-dasharray'
    },
    // 边线属性（旗帜）
    "lineAttributeCustomFlag": {//todo 底层统一一下字段，在此不要做兼容  jinming
      "colorTransparent": 'lineOpacity', // 透明度
      // "lineAfterArrow": '1', // 箭头头部（暂时写死）
      // "lineArrowSize": '1' // 箭头大小（暂时写死）
      "lineColor": 'color',
      "lineWidth": 'line-width'
    },
    'lineAttributeCustomCenterCircle': {
      "colorTransparent": 'line-opacity', // 透明度
      "lineColor": 'line-color',
      "lineWidth": 'line-width',
      // 'lineStyle': 'line-dasharray',
    },
    'lineAttributeCustomPolyline': {
      "colorTransparent": 'line-opacity', // 透明度
      "lineColor": 'line-color',
      "lineWidth": 'line-width',
      'lineStyle': 'line-style',
    }
  },
  /**
   * 根据特殊字符对键名进行校验
   * @param {*} key 传入的要进行校验的键名
   */
  correctKeyBySpecialKeyWords: function (key) {
    var keyCopy = JSON.parse(JSON.stringify(key))
    var index = keyCopy.indexOf('Custom')
    if (index > -1) {
      keyCopy = keyCopy.substr(0, index)
    }
    return keyCopy
  },
  /**
   * 根据style类型名称获得panel名
   * @param {*} cls 类型名称
   */
  getPanelByClass: function (cls) {
    // instanceof  
    if (QTMap[cls]) {
      for (var key in QTMapEditUtils.StylePanelDict) {
        if (QTMap[key]) {//有些传的是子类
          if (QTMap[cls].prototype && QTMap[key].prototype) {
            return QTMapEditUtils.StylePanelDict[key]
          }
        }
      }
    }
  },
  /**
   * 根据对象类型名称获得UI的item名与值--由对象到面板(用于初始化面板)
   * @param {*} cls 类型名称
   * @param {*} opt 对象数据
   */
  getUIItemAndValueByClass: function (cls, opt) {
    // instanceof

    var option = PlotMapUtils.deepClone(opt)
    // var styles = option.styles
    // if (styles) {//先将option与它的style里面的数据合并，这样方便下面的逻辑取值
    //   for (var i = 0; i < styles.length; i++) {
    //     option = Object.assign(option, styles[i].options)
    //   }
    // }
    // 
    var items = []
    if (QTMap[cls]) {
      for (var key in QTMapEditUtils.sdkItemToUIItem) {

        if (QTMap[key] && QTMap[cls] == QTMap[key]) {//有些传的是子类

          var keyItems = QTMapEditUtils.sdkItemToUIItem[key]//获取该style节点下的ui字段（ui上的大节点）

          var uiItemAndValue = {}//组织样式面板组件所需的数据，格式同样式面板组件的styleDefaultItemAttribute
          for (var i = 0; i < keyItems.length; i++) {
            var keyItem = this.correctKeyBySpecialKeyWords(keyItems[i])//处理键名，获得大节点名称
            uiItemAndValue[keyItem] = {}
            var uiItemObj = QTMapEditUtils.itemToPlotAttri[keyItems[i]]//ui上的大节点对象，panel的ui键名

            for (var uiItemKey in uiItemObj) {//遍历该ui字段对应的对象的字段，获取值
              //uiItemKey：panel下的小节点键名称
              if (!QTMapEditUtils.getExtraUIItem(uiItemAndValue, uiItemObj, uiItemKey, keyItem, option)) {//额外处理
                if (uiItemKey == 'lineStyle') {
                  uiItemAndValue[keyItem][uiItemKey] = option[uiItemObj[uiItemKey]] == [] ? [1, 0] : [1, 1]
                }
              }
              else {
                // uiItemAndValue[keyItem][uiItemKey] = option['properties'] ? option['properties'][uiItemObj[uiItemKey]] : option[uiItemObj[uiItemKey]]//将映射中的对象字段名替换为对象字段值
                uiItemAndValue[keyItem][uiItemKey] = option[uiItemObj[uiItemKey]]//将映射中的对象字段名替换为对象字段值
              }

            }
          }
          items.push(uiItemAndValue)//并存储返回

        }
      }


    }

    return items
  },
  /**
   * getUIItemAndValueByClass中有些需要被特殊对待的字段的处理写在这里
   * @param {*} uiItemAndValue 组织样式面板组件所需的数据，格式同样式面板组件的styleDefaultItemAttribute
   * @param {*} uiItemObj ui上的大节点对象，panel的ui
   * @param {*} uiItemKey panel下的小节点键名称
   * @param {*} keyItem 大节点名称
   * @param {*} option 对象数据
   * @returns 
   */
  getExtraUIItem: function (uiItemAndValue, uiItemObj, uiItemKey, keyItem, option) {
    if (uiItemObj[uiItemKey] == "label-anchor") {
      var anchor = QTMapEditUtils.anchorObjUIDict[option[uiItemObj[uiItemKey]]]
      uiItemAndValue[keyItem][uiItemKey] = anchor
      return false
    }
    else if (uiItemObj[uiItemKey] == 'line-dasharray' && option[uiItemObj[uiItemKey]]) {


      uiItemAndValue[keyItem][uiItemKey] = option[uiItemObj[uiItemKey]][1] == 0 ? '1' : '2'//如果dasharray第二个数是0，说明是实线
      return false
    }
    else if ((uiItemObj[uiItemKey]).toLowerCase && (uiItemObj[uiItemKey]).toLowerCase().indexOf("opacity") > -1) {//todo 存疑，不能直接指定key值吗
      uiItemAndValue[keyItem][uiItemKey] = option[uiItemObj[uiItemKey]] * 100
      return false
    }
    else if (uiItemKey == 'multiFlagLabelTableData') {
      if (Object.prototype.toString.call(uiItemObj[uiItemKey]) == '[object Array]') {//todo 存疑，不能直接指定key值吗
        var list = uiItemObj[uiItemKey]
        list.map((its, ins) => {
          its.text = option['properties'] ? option['properties'][uiItemObj[uiItemKey]] : option[uiItemObj[uiItemKey][ins].text]
        })
        uiItemAndValue[keyItem][uiItemKey] = list
        return false
      }
    } else if (uiItemKey == 'colorTransparent') {
      uiItemAndValue[keyItem][uiItemKey] = option[uiItemObj[uiItemKey]] * 100
      return false
    }

    else {
      return true
    }

  },
  /**
   * 根据吸管吸取的数据刷新当前UI，只替换值，不更改键
   * @param {*} objOpts 对象数据
   * @param {*} uiItems ui的项
   */
  updateUIItemAndValueByStraw: function (objOpts, uiItems) {


  },

  // [, , 'DIN Offc Pro Medium,Arial Unicode MS Bold', 'DIN Offc Pro Medium,Noto Sans CJK SC Medium,Arial Unicode MS Regular', 'DIN Offc Pro Regular,Noto Sans CJK SC DemiLight,Arial Unicode MS Regular', 'Heebo Medium,Arial Unicode MS Regular', 'Microsoft YaHei Regular', 'Microsoft YaHei Regular2', 'Noto Sans CJK SC Bold,Arial Unicode MS Regular', 'Noto Sans CJK SC DemiLight,Arial Unicode MS Regular', 'Noto Sans CJK SC Medium,Arial Unicode MS Regular', 'Open Sans Bold Italic,Arial Unicode MS Regular', 'Open Sans Regular,Arial Unicode MS Regular', 'simhei', 'WenCang Regular,Arial Unicode MS Regular', 'YouSheBiaoTiHei Regular,Arial Unicode MS Regular', 'YRDZST Bold,Arial Unicode MS Regular']
  labelTypes: ['黑体', '粗体', '斜体'],//字体选项
  //字体方位字典,ui-对象
  anchorUIObjDict: {
    "中": "center",
    "右": "left",
    "左": "right",
    "下": "top",
    "上": "bottom",
    "右下": "top-left",
    "左下": "top-right",
    "右上": "bottom-left",
    "左上": "bottom-right"
  },
  //字体方位字典,对象-ui
  anchorObjUIDict: {
    "center": "中",
    "left": "右",
    "right": "左",
    "top": "下",
    "bottom": "上",
    "top-left": "右下",
    "top-right": "左下",
    "bottom-left": "右上",
    "bottom-right": "左上"
  },
  /**
   * 根据ui的当前发生变化的数据，组织成对应的对象所需的格式
   * @param {*} itemKey ui上发生变化的项的名称
   * @param {*} value ui上发生变化的项的值
   * @param {*} other 辅助项
   * @returns 
   */
  getObjItemAndValueByUiItem: function (opt) {
    var itemKey = opt.key
    var value = opt.value
    var other = opt.other
    var opt = {}//准备给对象更新用的数据
    if (itemKey == 'outTextIsStroke') {

      if (!value) {
        opt["label-halo-width"] = -1
      }
      else {
        opt["label-halo-width"] = other ? other : 0
      }
    }
    else if (itemKey == 'outTextPosition') {
      opt['label-anchor'] = QTMapEditUtils.anchorUIObjDict[value]
    }
    else if (itemKey == 'colorTransparent') {
      opt['opacity'] = value * 0.01
    }
    else {
      var plotAttris = QTMapEditUtils.itemToPlotAttri
      for (var key in plotAttris) {
        var attri = plotAttris[key]
        for (var key2 in attri) {
          if (key2 == itemKey) {//ui的对上了
            opt[attri[key2]] = value//转化成对象的
          }
        }
      }
    }

    return opt
  }

}
